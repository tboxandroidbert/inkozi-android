package com.tbox.enkozi.Pojo;

/**
 * Created by apple on 28/06/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PositiveFeedbacks {

    @SerializedName("feedback1")
    @Expose
    private String feedback1;
    @SerializedName("feedback2")
    @Expose
    private String feedback2;
    @SerializedName("feedback3")
    @Expose
    private String feedback3;

    /**
     * @return The feedback1
     */
    public String getFeedback1() {
        return feedback1;
    }

    /**
     * @param feedback1 The feedback1
     */
    public void setFeedback1(String feedback1) {
        this.feedback1 = feedback1;
    }

    /**
     * @return The feedback2
     */
    public String getFeedback2() {
        return feedback2;
    }

    /**
     * @param feedback2 The feedback2
     */
    public void setFeedback2(String feedback2) {
        this.feedback2 = feedback2;
    }

    /**
     * @return The feedback3
     */
    public String getFeedback3() {
        return feedback3;
    }

    /**
     * @param feedback3 The feedback3
     */
    public void setFeedback3(String feedback3) {
        this.feedback3 = feedback3;
    }

}
