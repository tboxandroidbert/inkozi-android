package com.tbox.enkozi.Pojo;

/**
 * Created by apple on 28/06/16.
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.JsonPojo.UserInfoJson;


public class UserInfo {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    public Boolean is_login=false;
    private static final int MODE_PRIVATE = 0;

    private static UserInfo   _instance;
//    @SerializedName("api_secret_token")
//    @Expose
//    private String apiSecretToken;

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The apiSecretToken
     */
   // public String getApiSecretToken() {
       // return apiSecretToken;
  //  }

    /**
     * @param //apiSecretToken The api_secret_token
     */
//    //public void setApiSecretToken(String apiSecretToken) {
//        this.apiSecretToken = apiSecretToken;
//    }
    public static UserInfo getInstance(Context context,UserInfoJson infoJson) {
        if (_instance == null)
        {
            _instance = getUserInfo(context,infoJson);
        }
        return _instance;
    }
    private static UserInfo getUserInfo(Context context, UserInfoJson infoJson){
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences("USER_INFO",MODE_PRIVATE);
        String json = prefs.getString("USER_INFO", null);
        if(json != null){
            UserInfo userInfo = gson.fromJson(json, UserInfo.class);
            if(infoJson.getApiSecretToken().length() >0){
                userInfo.is_login=true;
            }
            return userInfo;

        }else{
            return new UserInfo();
        }

    }
    public void saveUserInfo(Context context,UserInfoJson infoJson){
        this.is_login=true;
        SharedPreferences prefs = context.getSharedPreferences("USER_INFO",MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(this);
        prefsEditor.putString("USER_INFO", json);
        prefsEditor.commit();
    }
    public void removeUserInfo(Context context,UserInfoJson infoJson){
        SharedPreferences prefs = context.getSharedPreferences("USER_INFO",MODE_PRIVATE);
        this.is_login=false;

        infoJson.setApiSecretToken("");
//        this.password="";
//        this.email_id="";

        Gson gson = new Gson();
        String json = gson.toJson(this);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("USER_INFO", json);
        prefsEditor.commit();
    }
    

}