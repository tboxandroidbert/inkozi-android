package com.tbox.enkozi.Pojo;
//
///**
// * Created by apple on 28/06/16.
// */
//
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//
//
//public class Attorney {
//
//    @SerializedName("advisor_id")
//    @Expose
//    private String advisorId;
//    @SerializedName("first_name")
//    @Expose
//    private String firstName;
//    @SerializedName("last_name")
//    @Expose
//    private String lastName;
//    @SerializedName("image")
//    @Expose
//    private String image;
//    @SerializedName("work_history")
//    @Expose
//    private String workHistory;
//    @SerializedName("graduation_year")
//    @Expose
//    private String graduationYear;
//    @SerializedName("positive_feedbacks")
//    @Expose
//    private List<Object> positiveFeedbacks = new ArrayList<Object>();
//
//    /**
//     *
//     * @return
//     * The advisorId
//     */
//    public String getAdvisorId() {
//        return advisorId;
//    }
//
//    /**
//     *
//     * @param advisorId
//     * The advisor_id
//     */
//    public void setAdvisorId(String advisorId) {
//        this.advisorId = advisorId;
//    }
//
//    /**
//     *
//     * @return
//     * The firstName
//     */
//    public String getFirstName() {
//        return firstName;
//    }
//
//    /**
//     *
//     * @param firstName
//     * The first_name
//     */
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    /**
//     *
//     * @return
//     * The lastName
//     */
//    public String getLastName() {
//        return lastName;
//    }
//
//    /**
//     *
//     * @param lastName
//     * The last_name
//     */
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    /**
//     *
//     * @return
//     * The image
//     */
//    public String getImage() {
//        return image;
//    }
//
//    /**
//     *
//     * @param image
//     * The image
//     */
//    public void setImage(String image) {
//        this.image = image;
//    }
//
//    /**
//     *
//     * @return
//     * The workHistory
//     */
//    public String getWorkHistory() {
//        return workHistory;
//    }
//
//    /**
//     *
//     * @param workHistory
//     * The work_history
//     */
//    public void setWorkHistory(String workHistory) {
//        this.workHistory = workHistory;
//    }
//
//    /**
//     *
//     * @return
//     * The graduationYear
//     */
//    public String getGraduationYear() {
//        return graduationYear;
//    }
//
//    /**
//     *
//     * @param graduationYear
//     * The graduation_year
//     */
//    public void setGraduationYear(String graduationYear) {
//        this.graduationYear = graduationYear;
//    }
//
//    /**
//     *
//     * @return
//     * The positiveFeedbacks
//     */
//    public List<Object> getPositiveFeedbacks() {
//        return positiveFeedbacks;
//    }
//
//    /**
//     *
//     * @param positiveFeedbacks
//     * The positive_feedbacks
//     */
//    public void setPositiveFeedbacks(List<Object> positiveFeedbacks) {
//        this.positiveFeedbacks = positiveFeedbacks;
//    }
//
//}
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Attorney {

    @SerializedName("advisor_id")
    @Expose
    private String advisorId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("video_link")
    @Expose
    private String videoLink;
    @SerializedName("brief_description")
    @Expose
    private String briefDescription;
    @SerializedName("curr_job_title")
    @Expose
    private String currJobTitle;
    @SerializedName("curr_employer")
    @Expose
    private String currEmployer;
    @SerializedName("work_history")
    @Expose
    private String workHistory;
    @SerializedName("graduation_year")
    @Expose
    private String graduationYear;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("lawtype")
    @Expose
    private String lawtype;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("feedbacks")
    @Expose
    private List<Feedback> feedbacks = new ArrayList<Feedback>();
    @SerializedName("ratings")
    @Expose
    private int ratings;

    /**
     *
     * @return
     * The advisorId
     */
    public String getAdvisorId() {
        return advisorId;
    }

    /**
     *
     * @param advisorId
     * The advisor_id
     */
    public void setAdvisorId(String advisorId) {
        this.advisorId = advisorId;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The videoLink
     */
    public String getVideoLink() {
        return videoLink;
    }

    /**
     *
     * @param videoLink
     * The video_link
     */
    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    /**
     *
     * @return
     * The briefDescription
     */
    public String getBriefDescription() {
        return briefDescription;
    }

    /**
     *
     * @param briefDescription
     * The brief_description
     */
    public void setBriefDescription(String briefDescription) {
        this.briefDescription = briefDescription;
    }

    /**
     *
     * @return
     * The currJobTitle
     */
    public String getCurrJobTitle() {
        return currJobTitle;
    }

    /**
     *
     * @param currJobTitle
     * The curr_job_title
     */
    public void setCurrJobTitle(String currJobTitle) {
        this.currJobTitle = currJobTitle;
    }

    /**
     *
     * @return
     * The currEmployer
     */
    public String getCurrEmployer() {
        return currEmployer;
    }

    /**
     *
     * @param currEmployer
     * The curr_employer
     */
    public void setCurrEmployer(String currEmployer) {
        this.currEmployer = currEmployer;
    }

    /**
     *
     * @return
     * The workHistory
     */
    public String getWorkHistory() {
        return workHistory;
    }

    /**
     *
     * @param workHistory
     * The work_history
     */
    public void setWorkHistory(String workHistory) {
        this.workHistory = workHistory;
    }

    /**
     *
     * @return
     * The graduationYear
     */
    public String getGraduationYear() {
        return graduationYear;
    }

    /**
     *
     * @param graduationYear
     * The graduation_year
     */
    public void setGraduationYear(String graduationYear) {
        this.graduationYear = graduationYear;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The lawtype
     */
    public String getLawtype() {
        return lawtype;
    }

    /**
     *
     * @param lawtype
     * The lawtype
     */
    public void setLawtype(String lawtype) {
        this.lawtype = lawtype;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The feedbacks
     */
    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    /**
     *
     * @param feedbacks
     * The feedbacks
     */
    public void setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    /**
     *
     * @return
     * The ratings
     */
    public int getRatings() {
        return ratings;
    }

    /**
     *
     * @param ratings
     * The ratings
     */
    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

}