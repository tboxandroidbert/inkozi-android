package com.tbox.enkozi.Pojo;

/**
 * Created by apple on 28/06/16.
 */

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("state_code")
    @Expose
    private String stateCode;
    @SerializedName("cities")
    @Expose
    private List<City> cities = new ArrayList<City>();

    /**
     * @return The stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName The state_name
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return The stateCode
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     * @param stateCode The state_code
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     * @return The cities
     */
    public List<City> getCities() {
        return cities;
    }

    /**
     * @param cities The cities
     */
    public void setCities(List<City> cities) {
        this.cities = cities;
    }

}
