package com.tbox.enkozi;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.AttorneyJson;
import com.tbox.enkozi.JsonPojo.Questions;
import com.tbox.enkozi.Pojo.Attorney;
import com.tbox.enkozi.Pojo.Feedback;
import com.tbox.enkozi.backend.AppController;
import com.tbox.enkozi.backend.AttorneyData;
import com.tbox.enkozi.backend.LawyerScreenEvent;
import com.tbox.enkozi.backend.RuntimePermissionHelper;
import com.tbox.enkozi.backend.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LawyerDetailActivity extends Activity implements View.OnClickListener,
        Callback<AttorneyJson> {
    private ImageView im_black1, im_black2, im_black3, im_black4, im_black5;
    private ImageView im_white1, im_white2, im_white3, im_white4, im_white5;
    private Button btn_back;
    private Button btn_like;
    private Button btn_msg;
    private Button btn_phn;
    private ImageView img_dot1, img_dot2, img_dot3, img_dot;
    private ImageView img_LawyerImage;
    private Button btn_uchkthanks, btn_uchkbest;
    private Button btn_chkthanks, btn_chkbest;
    private Button btn_emer;
    private TextView tv_name;
    private TextView tv_lawtype;
    private TextView tv_ansVia, tv_client, tv_chkBoxq1, tv_chkBoxq2, tv_query1, tv_query2,
            tv_query3;
    private Button btnVideo;
    int rating;
    ProgressDialog ringProgressDialog;
    String category = "";
    String city = "";
    String first = "";
    String second = "";
    String third = "";
    User usr = null;
    String categoryName = "";
    AttorneyData attData = null;
    AttorneyData checkAtt = null;
    List<Attorney> attorneyList = null;
    RuntimePermissionHelper runtimePermissionHelper;
    Boolean flag_callAttorney = false;
    Boolean flag_emergencycall = false;
    private RelativeLayout back_button;
    LayoutInflater inflater;
    View view;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lawyer_detail);
        // inflater = LayoutInflater.from(this);
        category = getIntent().getStringExtra("Category");
        city = getIntent().getStringExtra("City");
        categoryName = getIntent().getStringExtra("CategoryName");
        usr = User.getInstance(this);
        attData = AttorneyData.getInstance(this);
        first = getIntent().getStringExtra("one");
        second = getIntent().getStringExtra("two");
        third = getIntent().getStringExtra("three");
        if (usr.is_login) {
            getDataFromSharedPreference();
        } else {
            addDataInSharedPreference();
        }

        registerViews();
        setListners();
        if (checkInternetConnection()) {
            retrofitInitialization();
        } else {
            showInternetDialog();
        }

        setdata();


    }

    private void addDataInSharedPreference() {
        getSharedPreferences("BundleReplacementData", Context.MODE_PRIVATE).edit()
                .putString("category", category)
                .putString("city", city)
                .putString("categoryName", categoryName)
                .putString("first", first)
                .putString("second", second)
                .putString("third", third)
                .apply();
    }

    private void getDataFromSharedPreference() {
        SharedPreferences sharedPreferences = getSharedPreferences("BundleReplacementData",
                Context.MODE_PRIVATE);
        if (sharedPreferences.getString("swapscreen", " ").equals("yes")) {
            category = sharedPreferences.getString("category", " ");
            city = sharedPreferences.getString("city", "");
            categoryName = sharedPreferences.getString("categoryName", "");
            first = sharedPreferences.getString("first", "");
            second = sharedPreferences.getString("second", "");
            third = sharedPreferences.getString("third", "");

            getSharedPreferences("BundleReplacementData", Context.MODE_PRIVATE).edit()
                    .putString("category", " ")
                    .putString("city", " ")
                    .putString("categoryName", " ")
                    .putString("first", " ")
                    .putString("second", " ")
                    .putString("third", " ")
                    .putString("swapscreen", " ")
                    .apply();
        }


    }


    public void setdata() {

    }

    public void launchRingDialog() {
        ringProgressDialog = ProgressDialog.show(LawyerDetailActivity.this, "Please wait...",
                "Loading Details ...", true);
        ringProgressDialog.setCancelable(true);
        ringProgressDialog.show();

    }


    protected void registerViews() {
        back_button = (RelativeLayout) findViewById(R.id.back_button);
        btn_back = (Button) findViewById(R.id.back_b);
        btn_like = (Button) findViewById(R.id.fav_law);
        btn_msg = (Button) findViewById(R.id.btn_mes);
        btn_phn = (Button) findViewById(R.id.btn_call);
        img_dot = (ImageView) findViewById(R.id.dot);
        btnVideo = (Button) findViewById(R.id.videoButton);
        btn_uchkthanks = (Button) findViewById(R.id.btn_unclikthanks);
        btn_uchkbest = (Button) findViewById(R.id.btn_unclickbest);
        btn_chkthanks = (Button) findViewById(R.id.btn_clikthanks);
        btn_chkbest = (Button) findViewById(R.id.btn_clikbest);
        btn_emer = (Button) findViewById(R.id.btn_emer);
        img_LawyerImage = (ImageView) findViewById(R.id.lawyerImage);
        im_black1 = (ImageView) findViewById(R.id.black1);
        im_black2 = (ImageView) findViewById(R.id.black2);
        im_black3 = (ImageView) findViewById(R.id.black3);
        im_black4 = (ImageView) findViewById(R.id.black4);
        im_black5 = (ImageView) findViewById(R.id.black5);
        im_white1 = (ImageView) findViewById(R.id.white1);
        im_white2 = (ImageView) findViewById(R.id.white2);
        im_white3 = (ImageView) findViewById(R.id.white3);
        im_white4 = (ImageView) findViewById(R.id.white4);
        im_white5 = (ImageView) findViewById(R.id.white5);
        tv_lawtype = (TextView) findViewById(R.id.tv_lawtype);
        tv_name = (TextView) findViewById(R.id.tv_lawname);
        tv_chkBoxq1 = (TextView) findViewById(R.id.box1);
        tv_chkBoxq2 = (TextView) findViewById(R.id.box2);
        tv_query1 = (TextView) findViewById(R.id.query1);
        tv_query2 = (TextView) findViewById(R.id.query2);
        tv_query3 = (TextView) findViewById(R.id.query3);
        img_dot1 = (ImageView) findViewById(R.id.dot1);
        img_dot2 = (ImageView) findViewById(R.id.dot2);
        img_dot3 = (ImageView) findViewById(R.id.dot3);
        tv_ansVia = (TextView) findViewById(R.id.answerVia);
        tv_client = (TextView) findViewById(R.id.clientSaying);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeue-Light.otf");
        tv_chkBoxq1.setTypeface(helveticaNeueLTThin);
        tv_chkBoxq2.setTypeface(helveticaNeueLTThin);
        tv_query1.setTypeface(helveticaNeueLTThin);
        tv_query2.setTypeface(helveticaNeueLTThin);
        tv_ansVia.setTypeface(helveticaNeueLTThin);
        tv_client.setTypeface(helveticaNeueLTThin);
        tv_name.setTypeface(helveticaNeueLTThin);
        tv_lawtype.setTypeface(helveticaNeueLTThin);
        img_dot.setVisibility(View.INVISIBLE);

    }

    protected void setListners() {
        try {
            back_button.setOnClickListener(this);
            btn_back.setOnClickListener(this);
            btn_like.setOnClickListener(this);
            btn_msg.setOnClickListener(this);
            btn_phn.setOnClickListener(this);
            img_dot.setOnClickListener(this);
            btn_uchkthanks.setOnClickListener(this);
            btn_uchkbest.setOnClickListener(this);
            btn_chkthanks.setOnClickListener(this);
            btn_chkbest.setOnClickListener(this);
            btn_emer.setOnClickListener(this);
            im_black1.setOnClickListener(this);
            im_black2.setOnClickListener(this);
            im_black3.setOnClickListener(this);
            im_black4.setOnClickListener(this);
            im_black5.setOnClickListener(this);
            im_white1.setOnClickListener(this);
            im_white2.setOnClickListener(this);
            im_white3.setOnClickListener(this);
            im_white4.setOnClickListener(this);
            im_white5.setOnClickListener(this);
            btn_like.setOnClickListener(this);
            btnVideo.setOnClickListener(this);
            img_LawyerImage.setOnClickListener(this);
        } catch (Exception exp) {

            exp.printStackTrace();
        }
    }

    protected void sendEmail() {

        if (first.length() == 0 && second.length() == 0 && third.length() == 0) {
            Toast.makeText(LawyerDetailActivity.this, "Please select the question first",
                    Toast.LENGTH_LONG).show();
        } else {
            if (checkInternetConnection()) {
                retrofitQuestionInitialization();
            } else {
                showInternetDialog();
            }
        }

  /*      String email = attorneyList.get(0).getEmail();
        //String email = "junaid.tbox@gmail.com"; // test

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Client's mail to Lawyer");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Your selected questions are " + "\n\n" + first +
         "\n" + second + "\n" + third + "\n");
        startActivity(Intent.createChooser(emailIntent, "Sending your email..."));*/
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.videoButton: {
                if (checkInternetConnection()) {
                    String link = attorneyList.get(0).getVideoLink();
                    Intent intent = new Intent(LawyerDetailActivity.this, WebViewActivity.class);
                    intent.putExtra("link", link);
                    startActivity(intent);
                } else {
                    showInternetDialog();
                }
            }
            break;
            case R.id.back_button: {
                onBackPressed();
//                Intent intent = new Intent(LawyerDetailActivity.this,ConsultationActivity.class);
//                startActivity(intent);

            }
            case R.id.back_b: {
                onBackPressed();
//                Intent intent = new Intent(LawyerDetailActivity.this,ConsultationActivity.class);
//                startActivity(intent);

            }
            break;
            case R.id.btn_unclikthanks: {
                btn_uchkthanks.setVisibility(View.INVISIBLE);
                btn_chkthanks.setVisibility(View.VISIBLE);


            }
            break;
            case R.id.btn_unclickbest: {
                btn_uchkbest.setVisibility(View.INVISIBLE);
                btn_chkbest.setVisibility(View.VISIBLE);

            }
            break;
            case R.id.btn_clikthanks: {
                btn_chkthanks.setVisibility(View.INVISIBLE);
                btn_uchkthanks.setVisibility(View.VISIBLE);


            }
            break;
            case R.id.btn_clikbest: {
                btn_chkbest.setVisibility(View.INVISIBLE);
                btn_uchkbest.setVisibility(View.VISIBLE);

            }
            break;
//            case R.id.black5: {
//                im_black5.setVisibility(View.GONE);
//                im_white5.setVisibility(View.VISIBLE);
//            }
//            case R.id.black4: {
//                im_black4.setVisibility(View.GONE);
//                im_white4.setVisibility(View.VISIBLE);
//            }
//            case R.id.black3: {
//                im_black3.setVisibility(View.GONE);
//                im_white3.setVisibility(View.VISIBLE);
//            }
//            case R.id.black2: {
//                im_black2.setVisibility(View.GONE);
//                im_white2.setVisibility(View.VISIBLE);
//            }
//            case R.id.black1: {
//                im_black1.setVisibility(View.GONE);
//                im_white1.setVisibility(View.VISIBLE);
//            }
//            break;
//            case R.id.white1: {
//                im_white1.setVisibility(View.GONE);
//                im_black1.setVisibility(View.VISIBLE);
//            }
//            case R.id.white2: {
//                im_white2.setVisibility(View.GONE);
//                im_black2.setVisibility(View.VISIBLE);
//            }
//            case R.id.white3: {
//                im_white3.setVisibility(View.GONE);
//                im_black3.setVisibility(View.VISIBLE);
//            }
//            case R.id.white4: {
//                im_white4.setVisibility(View.GONE);
//                im_black4.setVisibility(View.VISIBLE);
//
//            }
//            case R.id.white5: {
//                im_white5.setVisibility(View.GONE);
//                im_black5.setVisibility(View.VISIBLE);
//
//            }
            //    break;
            case R.id.btn_call: {
                if (usr.is_login) {
                    // String ph = attorneyList.get(0).getPhone();
//                    String ph = "03454132017"; // test
//                    Intent intent = new Intent(Intent.ACTION_CALL);
//                    intent.setData(Uri.parse("tel:" + ph));
//                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission
// .CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                        // TODO: Consider calling
//                        //    ActivityCompat#requestPermissions
//                        // here to request the missing permissions, and then overriding
//                        //   public void onRequestPermissionsResult(int requestCode, String[]
// permissions,
//                        //                                          int[] grantResults)
//                        // to handle the case where the user grants the permission. See the
// documentation
//                        // for ActivityCompat#requestPermissions for more details.
//                        return;
//                    }
//                    startActivity(intent);
                    flag_callAttorney = true;
                    // askForPermission();
                    checkPermissions();
                } else {
                    EventBus.getDefault().post(new LawyerScreenEvent(" change bool"));
                    Intent intent = new Intent(LawyerDetailActivity.this, EnkoziMainActivity.class);
                    startActivity(intent);
                }

            }
            break;
            case R.id.btn_mes: {
                if (usr.is_login) {
                    sendEmail();

                } else {
                    EventBus.getDefault().post(new LawyerScreenEvent("change bool"));
                    Intent intent = new Intent(LawyerDetailActivity.this, EnkoziMainActivity.class);
                    startActivity(intent);
                }

            }
            break;
            case R.id.fav_law: {
                if (usr.is_login) {
                    open(view);
                } else {
//                    EventBus.getDefault().post(new LawyerScreenEvent(" change bool"));
//                    Intent intent = new Intent(LawyerDetailActivity.this, EnkoziMainActivity
// .class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
                }


            }
            break;
            case R.id.btn_emer: {
                String phone = getSharedPreferences("FAVATTORNEY", Context.MODE_PRIVATE).getString(
                        "PHONENO", "");
                // String phone = "03454132017"; // test

                if (phone.length() > 0) {
//                    String ph = phone;
//                    Intent intent = new Intent(Intent.ACTION_CALL);
//                    intent.setData(Uri.parse("tel:" + ph));
//                    startActivity(intent);
                    flag_emergencycall = true;
                    //askForPermission();
                    checkPermissions();
                } else {
                    Toast.makeText(LawyerDetailActivity.this, getString(R.string.nofavAttorney),
                            Toast.LENGTH_SHORT).show();
                }
            }
            break;

            case R.id.lawyerImage:{
                if(attorneyList!=null){
                    if(attorneyList.get(0).getImageUrl()!=null){
                        Intent intent = new Intent(LawyerDetailActivity.this,FullScreenImageViewActivity.class);
                        intent.putExtra("link",attorneyList.get(0).getImageUrl());
                        startActivity(intent);
                    }
                }
            }
            break;
        }

    }

    protected void open(View view) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Note");
        alertDialogBuilder.setMessage(
                "Are you sure,You want to add " + attorneyList.get(0).getFirstName()
                        + " as favourite Attorney");
        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                //attData.saveUserInfo(LawyerDetailActivity.this);
                getSharedPreferences("FAVATTORNEY", Context.MODE_PRIVATE).edit().putString(
                        "PHONENO", attorneyList.get(0).getPhone()).commit();

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialogBuilder.setCancelable(true);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void retrofitInitialization() {
        launchRingDialog();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);

        Call<AttorneyJson> call = enkInterface.getAttorneys(city, category);
        //asynchronous call
        call.enqueue(this);
    }

    public void onResponse(Call<AttorneyJson> call, Response<AttorneyJson> response) {
        int code = response.code();
        if (code == 200) {
            AttorneyJson user = response.body();
            Attorney atorney = user.getAttorneys();
            attorneyList = new ArrayList<Attorney>(1);
            attorneyList.add(atorney);
            for (int i = 0; i < attorneyList.size(); i++) {

                Log.e("response", "Got the Category: " + attorneyList.get(i).getFirstName());
            }
            tv_name.setText(attorneyList.get(0).getFirstName());

            tv_lawtype.setText(categoryName);
            List<Feedback> feedList = null;
            try {
                feedList = attorneyList.get(0).getFeedbacks();
                addFeedBack(feedList);
            } catch (Exception e) {
                e.getMessage();
            }
            if(feedList!=null) {
                for (int i=0;i<feedList.size();i++){
                    Log.e("feedLisr",feedList.get(i).getFeedback());
                }
            }
            addQueryQuestion();
            addRating();
            ringProgressDialog.dismiss();

            try {
                saveAttorneyData(attorneyList.get(0));
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else {
            Toast.makeText(this, "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }
    }

    protected void retrofitQuestionInitialization() {
        // launchRingDialog();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);

//        UserInfo userInfo = new UserInfo();
//        Attorney attorney = new Attorney();
//        String userID = userInfo.getUserId();
//        String advisorID = attorney.getAdvisorId();
        String userID = usr.user_id;
        String advisorID = attData.attorney_id;
        String quest = "";
        if (!first.equals("")) {
            quest = first;
            Log.e("Question is", quest);
        }
        if (!second.equals("")) {
            quest = second;
            Log.e("Question is", quest);
        }
        if (!third.equals("")) {
            quest = third;
            Log.e("Question is", quest);
        }


        Call<Questions> call = enkInterface.getQuestionResponse(userID, advisorID, quest);
        //asynchronous call
        call.enqueue(new Callback<Questions>() {
            @Override
            public void onResponse(Call<Questions> call, Response<Questions> response) {
                int code = response.code();
                if (code == 200) {
                    Questions ques = response.body();
                    Log.e("questionResponse", ques.toString());
                    if (ques.getStatus() == 1) {
                        //alert  suucess
                        Log.e("send", "yes");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(
                                LawyerDetailActivity.this);
                        builder.setMessage("Your Email has been sent!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        builder.setCancelable(true);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        Toast.makeText(LawyerDetailActivity.this,
                                getString(R.string.emailSendingFail), Toast.LENGTH_LONG).show();

                    }


                } else {
                    Toast.makeText(LawyerDetailActivity.this,
                            getString(R.string.emailSendingFail), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Questions> call, Throwable t) {
                //

                Toast.makeText(LawyerDetailActivity.this,
                        getString(R.string.emailSendingFail), Toast.LENGTH_LONG).show();

            }
        });
    }


    private void addRating() {
        int rate = attorneyList.get(0).getRatings();
        if (rate == 1) {
            im_black1.setVisibility(View.GONE);
            im_white1.setVisibility(View.VISIBLE);

        } else if (rating == 2) {
            im_black1.setVisibility(View.GONE);
            im_white1.setVisibility(View.VISIBLE);
            im_black2.setVisibility(View.GONE);
            im_white2.setVisibility(View.VISIBLE);

        } else if (rate == 3) {
            im_black1.setVisibility(View.GONE);
            im_white1.setVisibility(View.VISIBLE);
            im_black2.setVisibility(View.GONE);
            im_white2.setVisibility(View.VISIBLE);
            im_black3.setVisibility(View.GONE);
            im_white3.setVisibility(View.VISIBLE);
        } else if (rate == 4) {
            im_black1.setVisibility(View.GONE);
            im_white1.setVisibility(View.VISIBLE);
            im_black2.setVisibility(View.GONE);
            im_white2.setVisibility(View.VISIBLE);
            im_black3.setVisibility(View.GONE);
            im_white3.setVisibility(View.VISIBLE);
            im_black4.setVisibility(View.GONE);
            im_white4.setVisibility(View.VISIBLE);
        } else if (rate == 5) {
            im_black1.setVisibility(View.GONE);
            im_white1.setVisibility(View.VISIBLE);
            im_black2.setVisibility(View.GONE);
            im_white2.setVisibility(View.VISIBLE);
            im_black3.setVisibility(View.GONE);
            im_white3.setVisibility(View.VISIBLE);
            im_black4.setVisibility(View.GONE);
            im_white4.setVisibility(View.VISIBLE);
            im_black5.setVisibility(View.GONE);
            im_white5.setVisibility(View.VISIBLE);
        }


    }

    private void addFeedBack(List<Feedback> feed) {
        String first = feed.get(0).getFeedback();
        String second = feed.get(0).getFeedback();
        tv_chkBoxq1.setText(first);
        tv_chkBoxq2.setText(second);
    }

    private void addQueryQuestion() {
        try {
            if (first.equals("")) {
                img_dot1.setVisibility(View.GONE);
                tv_query1.setVisibility(View.GONE);
            } else {
                tv_query1.setText(first);
            }
            if (second.equals("")) {
                img_dot2.setVisibility(View.GONE);
                tv_query2.setVisibility(View.GONE);
            } else {
                tv_query2.setText(second);
            }
            if (third.equals("")) {
                img_dot3.setVisibility(View.GONE);
                tv_query3.setVisibility(View.GONE);
            } else {
                tv_query3.setText(third);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAttorneyData(Attorney attorney) throws IOException {
        attData.firstName = attorney.getFirstName();
        attData.lastName = attorney.getLastName();
        attData.attorney_id = attorney.getAdvisorId();
        attData.gradeYear = attorney.getGraduationYear();
        attData.image = attorney.getImage();
        attData.phone = attorney.getPhone();
        ImageLoader imageLoader1 = AppController.getInstance().getImageLoader();
        imageLoader1.get(attorney.getImageUrl(), ImageLoader.getImageListener(
                img_LawyerImage, R.drawable.atttorney_placeholder,
                R.drawable.atttorney_placeholder));
    }

    @Override
    public void onFailure(Call<AttorneyJson> call, Throwable t) {
        //  Toast.makeText(this, "Nope", Toast.LENGTH_LONG).show();
        Log.e("Error", "api");
        t.printStackTrace();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private void askForPermission() {
        String[] permissions = new String[]{Manifest.permission.CALL_PHONE};
        ActivityCompat.requestPermissions(this, permissions, 1);
    }


    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            runtimePermissionHelper = RuntimePermissionHelper.getInstance(this);
            if (runtimePermissionHelper.isAllMCPermissionAvailable()) {
// All permissions available. Go with the flow
                executeTask();
            } else {
// Few permissions not granted. Ask for ungranted permissions
                runtimePermissionHelper.setActivity(this);
                runtimePermissionHelper.requestPermissionsIfDenied(
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                executeTask();
                            }
                        }
                );
            }
        } else {
// SDK below API 23. Do nothing just go with the flow.
            executeTask();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
            int[] grantResults) {
        Log.e("onRequestPermmision", "in function present");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RuntimePermissionHelper.PERMISSION_REQUEST_CODE) {
            checkPermissions();
        }
    }

    private void executeTask() {


        if (flag_callAttorney) {
            String ph = attorneyList.get(0).getPhone();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + ph));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            flag_callAttorney = false;
            startActivity(intent);


        }
        if (flag_emergencycall) {
            String phone = getSharedPreferences("FAVATTORNEY", Context.MODE_PRIVATE).getString(
                    "PHONENO", "");
            if (phone.length() > 0) {
                String ph = phone;
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + ph));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                flag_emergencycall = false;
                startActivity(intent);
            }
        }
        Log.e("permission is set", "Permission is given now handle things");
//        if (!NetworkAvailabilityReceiver.isInternetAvailable(SplashActivity.this) && Utility
// .getUserName().length() > 0) {
//
//            Utility.allowAuthenticationDialog = true;
//            Utility.noNetwork = true;
//            Intent mIntent = new Intent(SplashActivity.this, BlurredActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(mIntent);
//        } else {
//            if (Utility.getUserName().length() > 0 && MainService.mService == null) {
//                Utility.startDialog(SplashActivity.this);
//                Utility.UserLoginStatus(SplashActivity.this, Utility.getEmail(), "", "", "",
// mValidateUserLoginInterface);
//            } else if (Utility.getUserName().length() > 0 && MainService.mService != null) {
//                Utility.reloginCalled = true;
//                startActivity(new Intent(SplashActivity.this, ContactActivity.class));
//                finish();
//                Utility.allowAuthenticationDialog = true;
//            } else {
//                mSharedPreferences = getSharedPreferences("mypre", 0);
//                text = mSharedPreferences.getString("conditionsAccepted", "");
//                new MyAsyncTask().execute();
//            }
//        }
    }

    private boolean checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable = true;
        } else {
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                LawyerDetailActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // do nothing
                                removeInternetDialog();
                              //  finish();
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }

}

