package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ForgetPassUserActivity extends Activity implements View.OnClickListener{
    Button btn_username;
    Button btn_pass;
    Button btn_usernameClick;
    Button btn_passClick;
    TextView tv_term;
    TextView tv_privacacy;
    Button btn_back;
    TextView tv_inkozi;
    private RelativeLayout back_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass_email);
        registerElement();
        setListner();
    }
    protected void registerElement(){
        back_button = (RelativeLayout) findViewById(R.id.back_button);
        btn_pass=(Button)findViewById(R.id.passwordbtn);
        btn_username=(Button) findViewById(R.id.userNamebtn);
        btn_passClick=(Button) findViewById(R.id.passwordbtnclick);
        btn_usernameClick=(Button) findViewById(R.id.userNamebtnclick);
        tv_privacacy=(TextView)findViewById(R.id.tv_privacy);
        tv_term=(TextView)findViewById(R.id.tv4);
        btn_back=(Button)findViewById(R.id.back);
        tv_inkozi=(TextView) findViewById(R.id.tv5);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        tv_term.setTypeface(helveticaNeueLTThin);
        tv_privacacy.setTypeface(helveticaNeueLTThin);
        tv_inkozi.setTypeface(helveticaNeueLTThin);
    }

    protected void setListner(){
        btn_pass.setOnClickListener(this);
        btn_username.setOnClickListener(this);
        tv_term.setOnClickListener(this);
        tv_privacacy.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        back_button.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.passwordbtn:
            {
                btn_pass.setVisibility(View.GONE);
                btn_passClick.setVisibility(View.VISIBLE);
                Intent intent = new Intent(ForgetPassUserActivity.this,ForgetPassActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
            break;
            case R.id.userNamebtn:
            {
                btn_username.setVisibility(View.GONE);
                btn_usernameClick.setVisibility(View.VISIBLE);
                Intent intent = new Intent(ForgetPassUserActivity.this,ForgetUserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.tv4:
            {
                Intent intent = new Intent(ForgetPassUserActivity.this,TermsAndServicesActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.tv_privacy:
            {
                Intent intent = new Intent(ForgetPassUserActivity.this,PrivacyPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.back:
            {
                Intent intent = new Intent(ForgetPassUserActivity.this,EnkoziMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            case R.id.back_button:
            {
                Intent intent = new Intent(ForgetPassUserActivity.this,EnkoziMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
        }

    }
    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        btn_usernameClick.setVisibility(View.GONE);
        btn_username.setVisibility(View.VISIBLE);
        btn_passClick.setVisibility(View.GONE);
        btn_pass.setVisibility(View.VISIBLE);

    }


}
