package com.tbox.enkozi;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.tbox.enkozi.ThirdParty.TapGestureListner;
import com.tbox.enkozi.ThirdParty.ZoomableDraweeView;

public class FullScreenImageViewActivity extends Activity {
    private boolean mAllowSwipingWhileZoomed = true;
    String uri="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uri = getIntent().getStringExtra("link");
        setContentView(R.layout.activity_full_screen_image_view);
        ZoomableDraweeView zoomableDraweeView =
                (ZoomableDraweeView) findViewById(R.id.zoomableView);
        zoomableDraweeView.setAllowTouchInterceptionWhileZoomed(mAllowSwipingWhileZoomed);
        zoomableDraweeView.setIsLongpressEnabled(false);
        TapGestureListner tapGestureListner = new TapGestureListner(zoomableDraweeView);
        zoomableDraweeView.setTapListener(tapGestureListner);
        zoomableDraweeView.getHierarchy().setProgressBarImage(R.drawable.placeholder_full);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .build();
        zoomableDraweeView.setController(null);
        zoomableDraweeView.setController(controller);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        FullScreenImageViewActivity.this.finish();
    }
}
