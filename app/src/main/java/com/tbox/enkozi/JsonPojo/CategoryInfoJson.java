package com.tbox.enkozi.JsonPojo;

/**
 * Created by apple on 01/07/16.
 */

import java.util.ArrayList;
import java.util.List;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.Pojo.CategoryInfo;
import com.tbox.enkozi.Pojo.CategoryQuestion;


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryInfoJson {

    @SerializedName("category_info")
    @Expose
    private CategoryInfo categoryInfo;
    @SerializedName("category_questions")
    @Expose
    private List<CategoryQuestion> categoryQuestions = new ArrayList<CategoryQuestion>();
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("error")
    @Expose
    private String error;

    /**
     *
     * @return
     * The categoryInfo
     */
    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    /**
     *
     * @param categoryInfo
     * The category_info
     */
    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    /**
     *
     * @return
     * The categoryQuestions
     */
    public List<CategoryQuestion> getCategoryQuestions() {
        return categoryQuestions;
    }

    /**
     *
     * @param categoryQuestions
     * The category_questions
     */
    public void setCategoryQuestions(List<CategoryQuestion> categoryQuestions) {
        this.categoryQuestions = categoryQuestions;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The error
     */
    public String getError() {
        return error;
    }

    /**
     *
     * @param error
     * The error
     */
    public void setError(String error) {
        this.error = error;
    }

}