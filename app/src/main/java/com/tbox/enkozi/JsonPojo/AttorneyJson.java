package com.tbox.enkozi.JsonPojo;

/**
 * Created by apple on 28/06/16.
 */


//import java.util.ArrayList;
//import java.util.List;
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//import com.tbox.enkozi.Pojo.Attorney;
//
//
//public class AttorneyJson {
//
//    @SerializedName("attorneys")
//    @Expose
//    private List<Attorney> attorneys = new ArrayList<Attorney>();
//    @SerializedName("status")
//    @Expose
//    private String status;
//    @SerializedName("error")
//    @Expose
//    private String error;
//
//    /**
//     * @return The attorneys
//     */
//    public List<Attorney> getAttorneys() {
//        return attorneys;
//    }
//
//    /**
//     * @param attorneys The attorneys
//     */
//    public void setAttorneys(List<Attorney> attorneys) {
//        this.attorneys = attorneys;
//    }
//
//    /**
//     * @return The status
//     */
//    public String getStatus() {
//        return status;
//    }
//
//    /**
//     * @param status The status
//     */
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    /**
//     * @return The error
//     */
//    public String getError() {
//        return error;
//    }
//
//    /**
//     * @param error The error
//     */
//    public void setError(String error) {
//        this.error = error;
//    }
//
//}


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.Pojo.Attorney;


public class AttorneyJson {

    @SerializedName("attorneys")
    @Expose
    private Attorney attorneys;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("error")
    @Expose
    private String error;

    /**
     *
     * @return
     * The attorneys
     */
    public Attorney getAttorneys() {
        return attorneys;
    }

    /**
     *
     * @param attorneys
     * The attorneys
     */
    public void setAttorneys(Attorney attorneys) {
        this.attorneys = attorneys;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The error
     */
    public String getError() {
        return error;
    }

    /**
     *
     * @param error
     * The error
     */
    public void setError(String error) {
        this.error = error;
    }

}