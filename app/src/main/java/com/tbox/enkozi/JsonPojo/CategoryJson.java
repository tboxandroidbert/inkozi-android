package com.tbox.enkozi.JsonPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.Pojo.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 28/06/16.
 */
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CategoryJson {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = new ArrayList<Category>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The categories
     */
    public ArrayList<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

}

