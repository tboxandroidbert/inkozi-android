package com.tbox.enkozi.JsonPojo;

/**
 * Created by apple on 28/06/16.
 */
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.Pojo.UserInfo;


public class UserInfoJson {

    @SerializedName("user_info")
    @Expose
    private UserInfo userInfo;


    @SerializedName("api_secret_token")
    @Expose
    private String apiSecretToken;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("error")
    @Expose
    private String error;



    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo The user_info
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    /**
     * @return The apiSecretToken
     */
     public String getApiSecretToken() {
     return apiSecretToken;
      }

    /**
     * @param apiSecretToken The api_secret_token
     */
public void setApiSecretToken(String apiSecretToken) {
        this.apiSecretToken = apiSecretToken;}







}