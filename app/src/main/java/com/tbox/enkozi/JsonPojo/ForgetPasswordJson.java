package com.tbox.enkozi.JsonPojo;

/**
 * Created by apple on 8/26/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetPasswordJson {


        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("error")
        @Expose
        private String error;
        @SerializedName("message")
        @Expose
        private String message;

        /**
         *
         * @return
         * The status
         */
        public Integer getStatus() {
            return status;
        }

        /**
         *
         * @param status
         * The status
         */
        public void setStatus(Integer status) {
            this.status = status;
        }

        /**
         *
         * @return
         * The error
         */
        public String getError() {
            return error;
        }

        /**
         *
         * @param error
         * The error
         */
        public void setError(String error) {
            this.error = error;
        }

        /**
         *
         * @return
         * The message
         */
        public String getMessage() {
            return message;
        }

        /**
         *
         * @param message
         * The message
         */
        public void setMessage(String message) {
            this.message = message;
        }
}
