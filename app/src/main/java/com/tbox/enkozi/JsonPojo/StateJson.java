package com.tbox.enkozi.JsonPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tbox.enkozi.Pojo.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 28/06/16.
 */

public class StateJson {


    @SerializedName("states")
    @Expose
    private List<State> states = new ArrayList<State>();
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("error")
    @Expose
    private String error;
    /**
     *
     * @return
     * The status
     *
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The error
     */
    public String getError() {
        return error;
    }

    /**
     *
     * @param error
     * The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     *
     * @return
     * The states
     */
    public List<State> getStates() {
        return states;
    }

    /**
     *
     * @param states
     * The states
     */
    public void setStates(List<State> states) {
        this.states = states;
    }

}