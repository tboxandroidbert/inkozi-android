package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.Api.LocationInterface;
import com.tbox.enkozi.JsonPojo.CategoryInfoJson;
import com.tbox.enkozi.JsonPojo.StateJson;
import com.tbox.enkozi.Pojo.CategoryQuestion;
import com.tbox.enkozi.Pojo.City;
import com.tbox.enkozi.Pojo.State;
import com.tbox.enkozi.backend.AppController;
import com.tbox.enkozi.backend.AttorneyData;
import com.tbox.enkozi.backend.RuntimePermissionHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsultationActivity extends Activity implements View.OnClickListener,
        Callback<StateJson>, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private boolean userIsInteracting;
    Spinner spin_selstate, spin_selcity;
    Button btn_uchkques1, btn_uchkques2, btn_uchkques3;
    Button btn_chkques1, btn_chkques2, btn_chkques3, btn_submit, btn_emg;
    Button btn_back, btn_submitClick;
    TextView tv_ques1, tv_ques2, tv_ques3;
    TextView tv_lawType, tv_lawCat, tv_selLegalQuest, tv_freeConsultation;
    TextView tv_selectLocation;
    List<String> categories = new ArrayList<String>();
    State stateObj = new State();
    City cityobj = new City();
    List<City> city = new ArrayList<City>();
    List<State> stateList = new ArrayList<>();
    List<CategoryQuestion> questionList = new ArrayList<>();
    CategoryQuestion questObj = new CategoryQuestion();
    String category = "";
    String cityName = "";
    String categoryName = "";
    int cityId = 0;
    AttorneyData checkAtt = null;
    ProgressDialog ringProgressDialog;
    String one, two, three = "";
    boolean first = false, second = false, third = false;
    ArrayList<String> arr_question;
    ArrayList<String> arr;
    ArrayList<String> arr_states;
    String state;
    ArrayAdapter<String> dataAdapter2;

    ArrayList<String> city_array;
    ArrayList<String> state_array;
    private boolean manual_selection = true;
    private ImageView loc_button;
    private RelativeLayout back_button;
    private boolean Location_Pressed = false;
    private ImageView loc_button_press;
    private boolean Pressed_Once = true;
    RuntimePermissionHelper runtimePermissionHelper;
    private FusedLocationProviderClient mFusedLocationClient;
    Location mLastLocation;
    private LocationManager manager;
    boolean isGpsDialogShowing = false;
    private AlertDialog gpsAlertDialog;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = AppController.getGoogleApiHelper().getGoogleApiClient();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);
        manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setContentView(R.layout.activity_consultation);
        if (mGoogleApiClient != null) {
            if (AppController.getGoogleApiHelper().isConnected()) {
                requestLoc();
            }

        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        category = getIntent().getStringExtra("Category");
        categoryName = getIntent().getStringExtra("CategoryName");

        stateObj.setStateName("Alaska");
        cityobj.setCity("Anchorage");
        cityobj.setCityId("67");
        city.add(cityobj);
        stateObj.setCities(city);
        stateList.add(stateObj);
        if (checkInternetConnection()) {
            retrofitInitializationStateWithCities();
        } else {
            showInternetDialog();
        }
        registerViews();
        setListner();
        if (checkInternetConnection()) {
            retrofitInitializationQuestion();
        }
    }

    public void launchRingDialog() {
        ringProgressDialog = ProgressDialog.show(ConsultationActivity.this,
                getString(R.string.pleasewate), getString(R.string.loading), true);
        ringProgressDialog.setCancelable(false);
        ringProgressDialog.show();

    }

    protected void retrofitInitializationStateWithCities() {
        launchRingDialog();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        // String lang= Locale.getDefault().toString();
        String country = "US";
        String lang = Locale.getDefault().toString();
        String sublang = lang.substring(0, 2);
        //String subLang=
        if (sublang.equals("es")) {
            lang = "es";
        } else {
            lang = "en";
        }
        Call<StateJson> call = enkInterface.getStatesWithCities(lang);
        //asynchronous call
        call.enqueue(this);
    }

    protected void registerViews() {
        spin_selstate = (Spinner) findViewById(R.id.spin_1);
        spin_selcity = (Spinner) findViewById(R.id.spin_2);
        btn_uchkques1 = (Button) findViewById(R.id.btn_remunchk_1);
        btn_uchkques2 = (Button) findViewById(R.id.btn_remunchk_2);
        btn_uchkques3 = (Button) findViewById(R.id.btn_remunchk_3);
        btn_chkques1 = (Button) findViewById(R.id.btn_remchk_1);
        btn_chkques2 = (Button) findViewById(R.id.btn_remchk_2);
        btn_chkques3 = (Button) findViewById(R.id.btn_remchk_3);
        btn_submitClick = (Button) findViewById(R.id.submitclick);
        btn_submit = (Button) findViewById(R.id.submit);
        btn_emg = (Button) findViewById(R.id.emg);
        btn_back = (Button) findViewById(R.id.back);
        back_button = (RelativeLayout) findViewById(R.id.back_button);
        tv_selectLocation = (TextView) findViewById(R.id.text_location);
        tv_ques1 = (TextView) findViewById(R.id.ques1);
        tv_ques2 = (TextView) findViewById(R.id.ques2);
        tv_ques3 = (TextView) findViewById(R.id.ques3);
        tv_lawType = (TextView) findViewById(R.id.lawtype);
        tv_lawCat = (TextView) findViewById(R.id.lawCategory);
        tv_selLegalQuest = (TextView) findViewById(R.id.selLegalQuest);
        tv_lawType.setText(categoryName);
        tv_freeConsultation = (TextView) findViewById(R.id.freeConsulation);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeue-Light.otf");
        tv_freeConsultation.setTypeface(helveticaNeueLTThin);
        tv_lawType.setTypeface(helveticaNeueLTThin);
        tv_selectLocation.setTypeface(helveticaNeueLTThin);
        tv_selLegalQuest.setTypeface(helveticaNeueLTThin);
        tv_lawCat.setTypeface(helveticaNeueLTThin);
        tv_ques1.setTypeface(helveticaNeueLTThin);
        tv_ques2.setTypeface(helveticaNeueLTThin);
        tv_ques3.setTypeface(helveticaNeueLTThin);


        loc_button = (ImageView) findViewById(R.id.loc_button);
        loc_button_press = (ImageView) findViewById(R.id.loc_button_press);


    }

    protected void setListner() {
        try {
            loc_button.setOnClickListener(this);
            loc_button_press.setOnClickListener(this);
            btn_emg.setOnClickListener(this);
            btn_submit.setOnClickListener(this);

            spin_selstate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    //                 if(userIsInteracting) {
                    try {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error: " + e.toString(),
                                Toast.LENGTH_LONG);
                    }

                    state = adapterView.getItemAtPosition(i).toString();
                    if (state.equals("Select state")) {
                        spin_selcity.setEnabled(false);
                    } else {
                        spin_selcity.setEnabled(true);
                    }
                    Log.e("item", state);
                    //      State obj = stateList.get(i);
                    State obj;
                    if (i != 0) {
                        obj = stateList.get(i - 1);
                    } else {
                        obj = stateList.get(i);
                    }
                    city = obj.getCities();
                    City cityObj = new City();
                    arr = new ArrayList<>(city.size());
                    arr.add("Select city");
                    for (int j = 0; j < city.size(); j++) {
                        arr.add(city.get(j).getCity());
                    }

                    if (manual_selection) {
                        dataAdapter2 = new ArrayAdapter<String>(ConsultationActivity.this,
                                R.layout.spinner_item, arr);
                        dataAdapter2.setDropDownViewResource(R.layout.simple_spinner_layout);
                        spin_selcity.setAdapter(dataAdapter2);
                        dataAdapter2.notifyDataSetChanged();
                    }
                    //                }

                }


                @Override

                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.e("Hello", "world");


                }
            });

            spin_selcity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    //        if(userIsInteracting) {
                    try {
//                        TextView tv = new TextView((Context) adapterView.getItemAtPosition(0));
//                        tv.setTextColor(Color.argb(0, 255, 0, 0));
                        ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error: " + e.toString(),
                                Toast.LENGTH_LONG);
                    }
//


//                    TextView tv=(TextView) adapterView.getChildAt(0);
//                    tv.setTextColor(Color.WHITE);
                    try {
                        cityName = adapterView.getItemAtPosition(i).toString();
                        cityId = i - 1;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //          }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.e("Hello", "world");


                }

            });
            btn_uchkques1.setOnClickListener(this);
            btn_uchkques2.setOnClickListener(this);
            btn_uchkques3.setOnClickListener(this);
            /*btn_chkques1.setOnClickListener(this);
            btn_chkques2.setOnClickListener(this);
            btn_chkques3.setOnClickListener(this);*/
            btn_back.setOnClickListener(this);
            btn_emg.setOnClickListener(this);
            back_button.setOnClickListener(this);
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }


    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.submit: {
                if (!state.equals("Select state")) {
                    if (!cityName.equals("Select city")) {
                        if (checkbool1() || checkbool2() || checkbool3()) {
                            btn_submit.setVisibility(View.GONE);

                            Intent intent = new Intent(ConsultationActivity.this,
                                    LawyerDetailActivity.class);
                            intent.putExtra("Category", category);
                            intent.putExtra("CategoryName", categoryName);
                            if (Location_Pressed) {
                                int index=-1;
                                for(int i=0;i<stateList.size();i++){
//                                    String a=spin_selstate.getSelectedView().toString();
//                                    String b=stateList.get(i).getStateName();
                                    if(stateList.get(i).getStateName().equals(state)){
                                        index=i;
                                        break;
                                    }
                                }
                                if(index!=-1){
                                    int indexC=-1;
                                    String dumCityName = cityName+" ";
                                    List<City> dumCity=new ArrayList<City>();
                                    dumCity = stateList.get(index).getCities();
                                    for(int i=0;i<dumCity.size();i++){
                                        String acd= dumCity.get(i).getCity();
                                        if(dumCity.get(i).getCity().equals(dumCityName)){
                                            //index=i;
                                            intent.putExtra("City", dumCity.get(i).getCityId());
                                            indexC=i;
                                            break;
                                        }
                                    }
                                    if(indexC==-1){
                                        intent.putExtra("City", city.get(0).getCityId());
                                    }

                                }else {
                                    intent.putExtra("City", city.get(0).getCityId());
                                }

                            } else {
                                intent.putExtra("City", city.get(cityId).getCityId());
                            }
                            btn_submitClick.setVisibility(View.VISIBLE);
                            if (checkbool1()) {
                                intent.putExtra("one", one);

                            } else {
                                intent.putExtra("one", "");
                            }
                            if (checkbool2()) {
                                //intent.putExtra("two", two);
                                intent.putExtra("one", two);
                                intent.putExtra("two", "");

                            } else {
                                intent.putExtra("two", "");
                            }
                            if (checkbool3()) {
                                // intent.putExtra("three", three);
                                intent.putExtra("one", three);
                                intent.putExtra("three", "");

                            } else {
                                intent.putExtra("three", "");
                            }
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Toast.makeText(ConsultationActivity.this,
                                    getString(R.string.selectQuestion),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ConsultationActivity.this, getString(R.string.selectCity),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ConsultationActivity.this, getString(R.string.selectState),
                            Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case R.id.back: {
                onBackPressed();
            }
            case R.id.back_button: {
                Intent intent = new Intent(ConsultationActivity.this, CategSelActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                onBackPressed();

            }
            break;

            case R.id.loc_button: {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    removeGpsDialog();
                    launchRingDialog();
                    CheckPermission();
                }

                // setAddressAuto();
            }
            break;
            case R.id.loc_button_press: {
                Location_Pressed=false;
                BackTrack();
            }
            break;
            case R.id.btn_remunchk_1: {


                btn_uchkques1.setVisibility(View.INVISIBLE);
                btn_chkques1.setVisibility(View.VISIBLE);

                btn_chkques2.setVisibility(View.INVISIBLE);
                btn_chkques3.setVisibility(View.INVISIBLE);
                try {
                    if (arr_question.get(1) != null && (!arr_question.get(1).equals(""))) {
                        btn_uchkques2.setVisibility(View.VISIBLE);
                    }
                } catch (IndexOutOfBoundsException e) {
                    btn_uchkques2.setVisibility(View.GONE);
                    btn_chkques2.setVisibility(View.GONE);
                }
                try {
                    if (arr_question.get(2) != null && (!arr_question.get(2).equals(""))) {
                        btn_uchkques3.setVisibility(View.VISIBLE);
                    }
                } catch (IndexOutOfBoundsException e) {
                    btn_uchkques3.setVisibility(View.GONE);
                    btn_chkques3.setVisibility(View.GONE);
                }
                first = true;
                second = false;
                third = false;

            }
            break;
            case R.id.btn_remchk_1: {
                btn_uchkques1.setVisibility(View.VISIBLE);
                btn_chkques1.setVisibility(View.INVISIBLE);

                first = false;

            }
            break;
            case R.id.btn_remunchk_2: {
                btn_chkques2.setVisibility(View.VISIBLE);
                btn_uchkques2.setVisibility(View.INVISIBLE);

                btn_chkques3.setVisibility(View.INVISIBLE);
                btn_chkques1.setVisibility(View.INVISIBLE);

                btn_uchkques1.setVisibility(View.VISIBLE);
                btn_uchkques3.setVisibility(View.VISIBLE);


                second = true;
                first = false;
                third = false;

            }
            break;
            case R.id.btn_remchk_2: {
                btn_chkques2.setVisibility(View.INVISIBLE);
                btn_uchkques2.setVisibility(View.VISIBLE);

                second = false;

            }
            break;
            case R.id.btn_remunchk_3: {
                btn_uchkques3.setVisibility(View.INVISIBLE);
                btn_chkques3.setVisibility(View.VISIBLE);

                btn_chkques2.setVisibility(View.INVISIBLE);
                btn_chkques1.setVisibility(View.INVISIBLE);

                btn_uchkques1.setVisibility(View.VISIBLE);
                btn_uchkques2.setVisibility(View.VISIBLE);

                third = true;
                first = false;
                second = false;

            }
            break;
            case R.id.btn_remchk_3: {
                btn_chkques3.setVisibility(View.INVISIBLE);
                btn_uchkques3.setVisibility(View.VISIBLE);

                btn_chkques1.setVisibility(View.INVISIBLE);
                btn_chkques2.setVisibility(View.INVISIBLE);

                third = false;

            }
            break;
            case R.id.emg: {
                String phone = getSharedPreferences("FAVATTORNEY", Context.MODE_PRIVATE).getString(
                        "PHONENO", "");

                if (phone.length() > 0) {
                    String ph = phone;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ph));
                    startActivity(intent);
                } else {
                    Toast.makeText(ConsultationActivity.this, getString(R.string.nofavAttorney),
                            Toast.LENGTH_SHORT).show();
                }
            }
            break;

        }

    }

    private void CheckPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            runtimePermissionHelper = RuntimePermissionHelper.getInstance(this);
            if (runtimePermissionHelper.isAllMCPermissionAvailable()) {
// All permissions available. Go with the flow
                setAddressAuto();
            } else {
// Few permissions not granted. Ask for ungranted permissions
                ringProgressDialog.cancel();
                runtimePermissionHelper.setActivity(this);
                runtimePermissionHelper.requestPermissionsIfDenied(
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //setAddressAuto();
                            }
                        }
                );
            }
        } else {
// SDK below API 23. Do nothing just go with the flow.

            setAddressAuto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        Log.e("onRequestPermmision", "in function present");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RuntimePermissionHelper.PERMISSION_REQUEST_CODE) {
            CheckPermission();
        }
    }


    private void BackTrack() {
        loc_button_press.setVisibility(View.GONE);
        loc_button.setVisibility(View.VISIBLE);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arr_states);
        stateAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
        spin_selstate.setAdapter(stateAdapter);


        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(ConsultationActivity.this,
                R.layout.spinner_item, arr);
        cityAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
        spin_selcity.setAdapter(cityAdapter);
        manual_selection = true;
    }


    private void progressShow() {


    }

    private void setAddressAuto() {
        class CurrentLocation implements LocationInterface {
            @Override
            public void getLocation(LocationInterface locationInterface, Location location) {
                String cityloc = "";
                String stateloc = "";
                Location_Pressed = true;
                manual_selection = false;
                spin_selcity.setEnabled(true);
                try {

                    if (location != null) {
                        double lat = location.getLatitude();
                        double lng = location.getLongitude();
                        Geocoder geoCoder = new Geocoder(ConsultationActivity.this,
                                Locale.getDefault());
                        StringBuilder builder = new StringBuilder();
                        try {
                            List<Address> address = geoCoder.getFromLocation(lat, lng, 1);
                            int maxLines = address.get(0).getMaxAddressLineIndex();
                            for (int i = 0; i < maxLines + 1; i++) {
                                String addressStr = address.get(0).getAddressLine(i);
                                builder.append(addressStr);
                                builder.append(" ");
                            }

                            String fnialAddress =
                                    builder.toString(); //This is the complete address.
                            try {

                                cityloc = String.valueOf(address.get(0).getLocality());
                                //stateloc = String.valueOf(address.get(0).getCountryName());
                                stateloc = String.valueOf(address.get(0).getAdminArea());
                                //            setCity(City);
                                //            setCountry(Country);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //        setCity(City);
                            //        setCountry(Country);


                            //  latituteField.setText(String.valueOf(lat));
                            //  longitudeField.setText(String.valueOf(lng));
                            //  addressField.setText(fnialAddress); //This will display the final
                            // address.
                            Log.i("CurrentLat", String.valueOf(lat));
                            Log.i("CurrentLong", String.valueOf(lat));
                            Log.i("CurrentAddress", String.valueOf(fnialAddress));
                        } catch (IOException e) {
                            // Handle IOException
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            // Handle NullPointerException
                            e.printStackTrace();
                        }
                        if (cityloc.equals("null")) {
                            cityloc = "No city found at this location.";
                        }


                        state_array = new ArrayList<>();
                        state_array.add(stateloc);
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(
                                ConsultationActivity.this,
                                android.R.layout.simple_spinner_item, state_array);
                        stateAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
                        spin_selstate.setAdapter(stateAdapter);


                        city_array = new ArrayList<>();
                        city_array.add(cityloc);
                        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(
                                ConsultationActivity.this,
                                R.layout.spinner_item, city_array);
                        cityAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
                        spin_selcity.setAdapter(cityAdapter);
                        cityAdapter.notifyDataSetChanged();
                        loc_button_press.setVisibility(View.VISIBLE);
                        loc_button.setVisibility(View.GONE);
                        ringProgressDialog.cancel();
                    } else {
                        ringProgressDialog.cancel();
                        if (!mGoogleApiClient.isConnected()) {
                            showAlert("Unable to Connect to Google so cant find Location");
                        } else {
                            requestLoc();
                            Toast.makeText(ConsultationActivity.this,
                                    "Unable to find Locaton Please try again.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        loc_button_press.setVisibility(View.GONE);
                        loc_button.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        CurrentLocation currentLocation = new CurrentLocation();
        getLastLocation(currentLocation);
    }

    private boolean checkbool1() {
        return first;
    }

    private boolean checkbool2() {
        return second;
    }

    private boolean checkbool3() {
        return third;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        //Location_Pressed = false;
        spin_selcity.setEnabled(false);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    public void onResponse(Call<StateJson> call, Response<StateJson> response) {
        int code = response.code();
        if (code == 200) {
            StateJson user = response.body();
            stateList = user.getStates();

            arr_states = new ArrayList<>(stateList.size());
            arr_states.add("Select state");
            for (int i = 0; i < stateList.size(); i++) {
                stateObj = stateList.get(i);
                arr_states.add(stateObj.getStateName());
                Log.e("response", "Got the states: " + stateList.get(
                        i).getStateName());//, Toast.LENGTH_LONG).show();
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, arr_states);
            dataAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
            // dataAdapter.add("this a hint");
            spin_selstate.setAdapter(dataAdapter);
            ringProgressDialog.dismiss();


        } else {
            //  Toast.makeText(this, "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG)
            // .show();
            Log.e("erorr", String.valueOf(code));
        }
    }

    @Override
    public void onFailure(Call<StateJson> call, Throwable t) {
        // Toast.makeText(this, "Nope", Toast.LENGTH_LONG).show();
        Log.e("erorr", "api response error");

    }

    protected void retrofitInitializationQuestion() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        String country = "US";
        String lang = Locale.getDefault().toString();
        String sublang = lang.substring(0, 2);
        //String subLang=
        if (sublang.equals("es")) {
            lang = "es";
        } else {
            lang = "en";
        }

        Call<CategoryInfoJson> call = enkInterface.getQuestions(category, lang);
        //asynchronous call
        call.enqueue(new Callback<CategoryInfoJson>() {
            @Override
            public void onResponse(Call<CategoryInfoJson> call,
                                   Response<CategoryInfoJson> response) {
                int code = response.code();
                if (code == 200) {

                    CategoryInfoJson question = response.body();
                    questionList = question.getCategoryQuestions();
                    arr_question = new ArrayList<>(questionList.size());
                    for (int i = 0; i < questionList.size(); i++) {
                        questObj = questionList.get(i);
                        arr_question.add(questObj.getQuestion());
                        Log.e("response", "Got the question: " + questionList.get(
                                i).getQuestion());//, Toast.LENGTH_LONG).show();
                    }

                    addList();
                } else {
                    //  Toast.makeText(ConsultationActivity.this, "Did not work: " + String
                    // .valueOf(code), Toast.LENGTH_LONG).show();
                    Log.e("error", String.valueOf(code));
                }

            }

            @Override
            public void onFailure(Call<CategoryInfoJson> call, Throwable t) {
                //  Toast.makeText(ConsultationActivity.this, "No question inflated", Toast
                // .LENGTH_LONG).show();
//                Log.e("api error",t.getMessage());
                ringProgressDialog.dismiss();
                disableGesture();

            }
        });
    }

    private void addList() {
        int count = 1;
        int size = arr_question.size();
        if (size != 0) {
            if (count == 1) {
                tv_ques1.setText(arr_question.get(0).toString());
                one = arr_question.get(0).toString();
                if (count < size) {
                    count++;
                }


            } else {
                btn_chkques1.setVisibility(View.GONE);
                btn_uchkques1.setVisibility(View.GONE);
                tv_ques1.setVisibility(View.GONE);

            }
            if (count == 2) {
                tv_ques2.setText(arr_question.get(1).toString());
                two = arr_question.get(1).toString();
                if (count < size) {
                    count++;
                }
            } else {
                btn_chkques2.setVisibility(View.GONE);
                btn_uchkques2.setVisibility(View.GONE);
                tv_ques2.setVisibility(View.GONE);
            }
            if (count == 3) {
                tv_ques3.setText(arr_question.get(2).toString());
                three = arr_question.get(2).toString();
            } else {
                btn_chkques3.setVisibility(View.GONE);
                btn_uchkques3.setVisibility(View.GONE);
                tv_ques3.setVisibility(View.GONE);
            }
        } else {
            btn_chkques1.setVisibility(View.INVISIBLE);
            btn_uchkques1.setVisibility(View.INVISIBLE);
            tv_ques1.setVisibility(View.INVISIBLE);
            btn_chkques3.setVisibility(View.INVISIBLE);
            btn_uchkques3.setVisibility(View.INVISIBLE);
            tv_ques3.setVisibility(View.INVISIBLE);
            btn_chkques2.setVisibility(View.INVISIBLE);
            btn_uchkques2.setVisibility(View.INVISIBLE);
            tv_ques2.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        btn_submitClick.setVisibility(View.GONE);
        btn_submit.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            Log.e("mLastLocaton", mLastLocation.getLatitude() + "");
                            Log.e("mLastLocaton", mLastLocation.getLongitude() + "");
                        } else {
                            Log.e("no location Found", "yupe");
                        }
                    }
                });
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation(final LocationInterface locationInterface) {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            locationInterface.getLocation(locationInterface,
                                    mLastLocation = task.getResult());
                        } else {
                            locationInterface.getLocation(locationInterface, null);
                        }
                    }
                });
    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                ConsultationActivity.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle(getString(R.string.dialog_no_gps))
                .setMessage(getString(R.string.dialog_no_gps_messgae))
                .setPositiveButton(getString(R.string.dialog_enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                finish();

                            }
                        });
        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    private void disableGesture() {
        ArrayList<String> arr_states = new ArrayList<>();
        arr_states.add("Select state");
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arr_states);
        stateAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
        spin_selstate.setEnabled(false);
        spin_selstate.setClickable(false);
        spin_selstate.setAdapter(stateAdapter);

        ArrayList<String> arr = new ArrayList<>();
        arr_states.add("Select city");
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(ConsultationActivity.this,
                R.layout.spinner_item, arr);
        cityAdapter.setDropDownViewResource(R.layout.simple_spinner_layout);
        spin_selcity.setEnabled(false);
        spin_selcity.setClickable(false);
        spin_selcity.setAdapter(cityAdapter);
        // remove views
        btn_chkques1.setVisibility(View.INVISIBLE);
        btn_uchkques1.setVisibility(View.INVISIBLE);
        tv_ques1.setVisibility(View.INVISIBLE);
        btn_chkques3.setVisibility(View.INVISIBLE);
        btn_uchkques3.setVisibility(View.INVISIBLE);
        tv_ques3.setVisibility(View.INVISIBLE);
        btn_chkques2.setVisibility(View.INVISIBLE);
        btn_uchkques2.setVisibility(View.INVISIBLE);
        tv_ques2.setVisibility(View.INVISIBLE);
        //remove listner
        loc_button.setClickable(false);
    }

    private boolean checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable = true;
        } else {
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                ConsultationActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeInternetDialog();
                                //finish();
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected(@Nullable Bundle bundle) {
        if(ringProgressDialog.isShowing()){
            ringProgressDialog.dismiss();
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        if(ringProgressDialog.isShowing()){
            ringProgressDialog.dismiss();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(ringProgressDialog.isShowing()){
            ringProgressDialog.dismiss();
        }
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i("dummy", "Location services connection failed with code "
                    + connectionResult.getErrorCode());
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private void showAlert(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        launchRingDialog();
                        if (mGoogleApiClient != null) {
                            mGoogleApiClient.connect();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void handleNewLocation(Location location) {
        Log.e("Location", location.toString());
    }

    private void requestLoc() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

}