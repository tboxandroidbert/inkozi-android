package com.tbox.enkozi;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;

public class TermActivity extends Activity {

    TextView tv_term;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
//        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
//        scrollView.setBackgroundColor(Color.WHITE);
        tv_term=(TextView)findViewById(R.id.termOfUse);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd-Th.otf");
        tv_term.setTypeface(helveticaNeueLTThin);


    }
}
