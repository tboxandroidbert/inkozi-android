package com.tbox.enkozi;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class PrivacyPolicyActivity extends Activity {
    TextView tv_privacay;
    TextView tv_quest1,tv_quest2,tv_quest3,tv_quest4,tv_quest5,tv_quest6,tv_quest7,tv_quest8,tv_quest9,tv_quest10,
            tv_quest11,tv_quest12,tv_quest13;
    TextView tv_ans1,tv_ans2,tv_ans3,tv_ans4,tv_ans5,tv_ans6,tv_ans7,tv_ans8,tv_ans9,tv_ans10,tv_ans11,tv_ans12,tv_ans13;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        registerElement();
    }
    private void registerElement()
    {
        tv_privacay=(TextView)findViewById(R.id.privacy);
        tv_quest1=(TextView)findViewById(R.id.question1);
        tv_quest2=(TextView)findViewById(R.id.question2);
        tv_quest3=(TextView)findViewById(R.id.question3);
        tv_quest4=(TextView)findViewById(R.id.question4);
        tv_quest5=(TextView)findViewById(R.id.question5);
        tv_quest6=(TextView)findViewById(R.id.question6);
        tv_quest7=(TextView)findViewById(R.id.question7);
        tv_quest8=(TextView)findViewById(R.id.question8);
        tv_quest9=(TextView)findViewById(R.id.question9);
        tv_quest10=(TextView)findViewById(R.id.question10);
        tv_quest11=(TextView)findViewById(R.id.question11);
        tv_quest12=(TextView)findViewById(R.id.question12);
        tv_quest13=(TextView)findViewById(R.id.question13);
        tv_ans1=(TextView)findViewById(R.id.ans1);
        tv_ans2=(TextView)findViewById(R.id.ans2);
        tv_ans3=(TextView)findViewById(R.id.ans3);
        tv_ans4=(TextView)findViewById(R.id.ans4);
        tv_ans5=(TextView)findViewById(R.id.ans5);
        tv_ans6=(TextView)findViewById(R.id.ans6);
        tv_ans7=(TextView)findViewById(R.id.ans7);
        tv_ans8=(TextView)findViewById(R.id.ans8);
        tv_ans9=(TextView)findViewById(R.id.ans9);
        tv_ans10=(TextView)findViewById(R.id.ans10);
        tv_ans11=(TextView)findViewById(R.id.ans11);
        tv_ans12=(TextView)findViewById(R.id.ans12);
        tv_ans13=(TextView)findViewById(R.id.ans13);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd-Th.otf");
//        Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
//        Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        tv_privacay.setTypeface(helveticaNeueLTThin);







    }
}
