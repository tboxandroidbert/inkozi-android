package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.CategoryJson;
import com.tbox.enkozi.Pojo.Category;
import com.tbox.enkozi.backend.AttorneyData;
import com.tbox.enkozi.backend.ListViewAdapter;
import com.tbox.enkozi.backend.User;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CategSelActivity extends Activity implements View.OnClickListener, Callback<CategoryJson> {

    Button btn_back;
    Button btn_unchkdui;
    Button btn_chkdui;
    Button btn_unchkcar;
    Button btn_chkcar;
    Button btn_unchkimigration;
    Button btn_chkimigration;
    Button btn_unchkdom;
    Button btn_chkdom;
    Button btn_emg;
    Button btn_signout;
    Button btn_signoutClick;
    private ListView listView;
    private ListViewAdapter adapter;
    private ArrayList<Category> categoryList;
    AttorneyData  checkAtt= null;
    TextView tv_selCategory;
    User usr =null;
    ProgressDialog ringProgressDialog;
    RelativeLayout back_button;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categ_sel);
        registerView();
        if(checkInternetConnection()) {
            retrofitInitialization();
        }else{
            showInternetDialog();
        }
        usr=User.getInstance(this);
        setListners();
        checkUserLogin();

    }
    public void launchRingDialog() {
        ringProgressDialog = ProgressDialog.show(CategSelActivity.this, getString(R.string.pleasewate), getString(R.string.loading), true);
        ringProgressDialog.setCancelable(false);
        ringProgressDialog.show();

    }
    public  void  checkUserLogin()
    {

        if(usr.is_login)
        {
            btn_back.setVisibility(View.INVISIBLE);
            btn_signout.setVisibility(View.VISIBLE);
        }
        else{

            btn_signout.setVisibility(View.GONE);
            btn_back.setVisibility(View.VISIBLE);

        }
    }
    protected void retrofitInitialization(){
        launchRingDialog();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        String country="US";
        String lang= Locale.getDefault().toString();
        String sublang= lang.substring(0,2);
        //String subLang=
        if(sublang.equals("es"))
        {
            lang="es";
        }
        else
        {
            lang="en";
        }
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        Call<CategoryJson> call = enkInterface.getCategories(country,lang);

        //asynchronous call
        call.enqueue(this);

    }

    protected void registerView() {
        btn_back = (Button) findViewById(R.id.back);
        back_button = (RelativeLayout) findViewById(R.id.back_button);
        btn_chkcar = (Button) findViewById(R.id.btn_clickCar);
        btn_chkdui = (Button) findViewById(R.id.btn_clickDUI);
        btn_chkimigration = (Button) findViewById(R.id.btn_clickImigration);
        btn_chkdom = (Button) findViewById(R.id.btn_clickdomDis);
        btn_unchkcar = (Button) findViewById(R.id.btn_unCar);
        btn_unchkdom = (Button) findViewById(R.id.btn_undomDis);
        btn_unchkdui = (Button) findViewById(R.id.btn_udui);
        btn_unchkimigration = (Button) findViewById(R.id.btn_unImigration);
        btn_signout=(Button) findViewById(R.id.signout);
        btn_signoutClick=(Button) findViewById(R.id.signoutClick);
        tv_selCategory=(TextView) findViewById(R.id.selCate);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        tv_selCategory.setTypeface(helveticaNeueLTThin);
        btn_emg = (Button) findViewById(R.id.btn_emer);
        listView = (ListView) findViewById(R.id.list);
        listView.setFocusable(false);
    }

    protected void setListners() {
        try {
            back_button.setOnClickListener(this);
            btn_back.setOnClickListener(this);
            btn_chkcar.setOnClickListener(this);
            btn_chkdui.setOnClickListener(this);
            btn_chkimigration.setOnClickListener(this);
            btn_chkdom.setOnClickListener(this);
            btn_unchkcar.setOnClickListener(this);
            btn_unchkdom.setOnClickListener(this);
            btn_unchkdui.setOnClickListener(this);
            btn_unchkimigration.setOnClickListener(this);
            btn_emg.setOnClickListener(this);
            btn_signout.setOnClickListener(this);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent=new Intent(CategSelActivity.this,ConsultationActivity.class);
                    intent.putExtra("Category",categoryList.get(i).getCategoryId());
                    Log.e("selected category id",categoryList.get(i).getCategoryId());
                    intent.putExtra("CategoryName",categoryList.get(i).getCategoryName());
                    Log.e("selected category name",categoryList.get(i).getCategoryName());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }
            });

        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(CategSelActivity.this, EnkoziMainActivity.class);
        intent.putExtra("SignOut","Back");
        startActivity(intent);*/
        if(btn_signout.getVisibility() == View.VISIBLE){
            this.finish();
        }else if(btn_signout.getVisibility() == View.GONE){
           /* Intent intent = new Intent(CategSelActivity.this,EnkoziMainActivity.class);
            startActivity(intent);
            this.finish();*/
           super.onBackPressed();
        }
      //  super.onBackPressed();

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_unCar: {
                btn_unchkcar.setVisibility(View.GONE);
                btn_chkcar.setVisibility(View.VISIBLE);
                Intent intent=new Intent(CategSelActivity.this,ConsultationActivity.class);
                intent.putExtra("Category","7");
                Log.e("selected category id","7");
                intent.putExtra("CategoryName","Personal Injury Law");
                Log.e("selected category name","Personal Injury Law");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);


            }
            break;
            case R.id.btn_clickCar: {
                btn_chkcar.setVisibility(View.GONE);
                btn_unchkcar.setVisibility(View.VISIBLE);

            }
            break;
            case R.id.btn_clickdomDis: {
                btn_chkdom.setVisibility(View.GONE);
                btn_unchkdom.setVisibility(View.VISIBLE);

            }
            break;
            case R.id.btn_undomDis: {
                btn_unchkdom.setVisibility(View.GONE);
                btn_chkdom.setVisibility(View.VISIBLE);
                Intent intent=new Intent(CategSelActivity.this,ConsultationActivity.class);
                intent.putExtra("Category","46");
                Log.e("selected category id","46");
                intent.putExtra("CategoryName","Family law-Divorce & Custody");
                Log.e("selected category name","Family law-Divorce & Custody");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.btn_clickDUI: {
                btn_chkdui.setVisibility(View.GONE);
                btn_unchkdui.setVisibility(View.VISIBLE);
            }
            break;
            case R.id.btn_udui: {
                btn_unchkdui.setVisibility(View.GONE);
                btn_chkdui.setVisibility(View.VISIBLE);
                Intent intent=new Intent(CategSelActivity.this,ConsultationActivity.class);
                intent.putExtra("Category","74");
                Log.e("selected category id","74");
                intent.putExtra("CategoryName","Driving Under Influence Of Alcohol");
                Log.e("selected category name","Driving Under Influence Of Alcoho");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.btn_unImigration: {
                btn_unchkimigration.setVisibility(View.GONE);
                btn_chkimigration.setVisibility(View.VISIBLE);
                Intent intent=new Intent(CategSelActivity.this,ConsultationActivity.class);
                intent.putExtra("Category","2");
                Log.e("selected category id","2");
                intent.putExtra("CategoryName","Immigration Law");
                Log.e("selected category name","Immigration Law");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
            break;
            case R.id.btn_clickImigration: {
                btn_chkimigration.setVisibility(View.GONE);
                btn_unchkimigration.setVisibility(View.VISIBLE);


            }
            break;
            case R.id.back: {


                Intent intent = new Intent(CategSelActivity.this, EnkoziMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                this.finish();

            }
            case R.id.back_button: {


                Intent intent = new Intent(CategSelActivity.this, EnkoziMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                this.finish();

            }
            break;

            case R.id.btn_emer: {
                String phone=  getSharedPreferences("FAVATTORNEY", Context.MODE_PRIVATE).getString("PHONENO","");

                if (phone.length() > 0) {
                    String ph = phone;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ph));
                    startActivity(intent);
                } else {
                    Toast.makeText(CategSelActivity.this,  getString(R.string.nofavAttorney), Toast.LENGTH_SHORT).show();
                }
                }
            break;
            case R.id.signout: {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage(getString(R.string.logout_message));
                        alertDialogBuilder.setPositiveButton(getString(R.string.yes),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        usr.removeUserInfo(getApplicationContext());
                                        checkAtt= AttorneyData.getInstance(getApplicationContext());
                                        checkAtt.removeAttorneyInfo(getApplicationContext());
                                        btn_signout.setVisibility(View.GONE);
                                        btn_signoutClick.setVisibility(View.VISIBLE);
                                        Intent intent = new Intent(CategSelActivity.this,EnkoziMainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                  alertDialogBuilder.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();




            }

            }

        }


    public void onResponse(final Call<CategoryJson> call, Response<CategoryJson> response) {
        int code = response.code();
        if (code == 200) {
            CategoryJson user = response.body();
            categoryList = user.getCategories();
//            for (int i = 0; i < categoryList.size(); i++) {
//               // categoryList.add(categoryList.get(i));
//                Log.e("response", "Got the Category: " + categoryList.get(i).getCategoryName());//, Toast.LENGTH_LONG).show();
//            }
            adapter = new ListViewAdapter(this, R.layout.catlistlayout, categoryList);
            listView.setAdapter(adapter);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                public void run() {
                    ringProgressDialog.dismiss();
                }
            }, 1000);


        } else {
          //  Toast.makeText(this, "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG).show();
            Log.e("error",String.valueOf(code));
            ringProgressDialog.dismiss();
        }

    }

    @Override
    public void onFailure(Call<CategoryJson> call, Throwable t) {
     //   Toast.makeText(this, "Nope", Toast.LENGTH_LONG).show();
        Log.e("error","api error");
        ringProgressDialog.dismiss();
    }

    public void onRestart() {
        super.onRestart();
//        When BACK BUTTON is pressed, the activity on the stack is restarted
//        Do what you want on the refresh procedure here
        btn_chkcar.setVisibility(View.GONE);
        btn_unchkcar.setVisibility(View.VISIBLE);
        btn_unchkimigration.setVisibility(View.VISIBLE);
        btn_chkimigration.setVisibility(View.GONE);
        btn_unchkdom.setVisibility(View.VISIBLE);
        btn_chkdom.setVisibility(View.GONE);
        btn_unchkdui.setVisibility(View.VISIBLE);
        btn_chkdui.setVisibility(View.GONE);
        checkUserLogin();
    }

    private boolean checkInternetConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable= true;
        }else{
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                CategSelActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // do nothing
                                removeInternetDialog();
                                finish();
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }
}

