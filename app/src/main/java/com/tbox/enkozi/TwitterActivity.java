package com.tbox.enkozi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.UserInfoJson;
import com.tbox.enkozi.Pojo.Category;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import retrofit.client.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class TwitterActivity extends Activity {
    private TwitterLoginButton loginButton;
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "UGZSL5MA2qVUuBc36ECoWmR9W";
    private static final String TWITTER_SECRET = "7wJwWbcgf3e7P0MPqgYHijaWJ1tMnYqtjxupNw51URoN0V94Rq";
    public static final String PREFS_TWITTER_NAME = "MyPrefsTwitterFile";
    private static final String PREF_Email = "email";
    private static final String PREF_Id = "id";
    private static final String PREF_NAme = "username";
    private static final String PREF_Token = "token";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    TwitterSession session;
    com.tbox.enkozi.backend.User usr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_twitter);
        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {


                // The TwitterSession is also available through:
                // Twitter.getInstance().core.getSessionManager().getActiveSession()
                session = result.data;
                final TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;


                String username = result.data.getUserName();
                AccountService ac = Twitter.getApiClient(result.data).getAccountService();
                ac.verifyCredentials(true, true, new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        String email = result.data.email;
                        Long userid = result.data.id;
                        String name = result.data.name;
                        String username = result.data.screenName;

                        final JSONObject object = new JSONObject();
                        try {
                            object.put("email",email);
                            object.put("userid",userid);
                            object.put("name",name);
                            object.put("username",username);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("Credentials",object.toString());

                       final TwitterAuthClient authClient = new TwitterAuthClient();
                        authClient.requestEmail(session, new Callback<String>() {

                            @Override
                            public void success(Result<String> result) {
                                // Do something with the result, which provides the email address
                                //     email[0] = result.data;
                                String uemail = result.data;
                                Log.e("twitterEmail", uemail);
                                try {
                                    object.put("email",uemail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    String getemail = object.getString("email");
                                    String getname = object.getString("name");
                                    String getid = object.getString("userid");
                                    String getidtoken = object.getString("userid");
                                    setTwitterData(getid,getname,getemail,getidtoken);
                                    retrofitInitializationtwitter(getid,getname,getemail,getidtoken);
                            //        deleteSharedPreferences(PREFS_TWITTER_NAME);
                                    deletedata();
                                    logoutTwitter();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void failure(TwitterException exception) {
                                // Do something on failure

                                Log.e("error","errpo");
                            }
                        });


                    }

                    @Override
                    public void failure(TwitterException exception) {

                    }
                });

                // TODO: Remove toast and use the TwitterSession's userID
                // with your app's user model
//                String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
//                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
//               int login = session.hashCode();
//                Intent intent =new Intent(TwitterActivity.this,CategSelActivity.class);
//                startActivity(intent);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
//
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
//        usr = com.tbox.enkozi.backend.User.getInstance(this);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Make sure that the loginButton hears the result from any
        // Activity that it triggered.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }

@Override
protected void onStart()
{
    super.onStart();
    loginButton.performClick();
}
    protected void setUser(UserInfoJson infoJson) {
        usr.api_secret_token = infoJson.getApiSecretToken();
        usr.first_name = infoJson.getUserInfo().getFirstName();
        usr.last_name = infoJson.getUserInfo().getLastName();
        // usr.phone = infoJson.getUserInfo().getPhone();
        usr.email = infoJson.getUserInfo().getEmail();
        usr.username = infoJson.getUserInfo().getUsername();
        usr.user_id = infoJson.getUserInfo().getUserId();
    }

    @Override
    public void onBackPressed() {

       /* finish();

        */
        /*super.onBackPressed();*/
        deletedata();
        Intent intent = new Intent(TwitterActivity.this,EnkoziMainActivity.class);
        startActivity(intent);
        TwitterActivity.this.finish();
        Intent intent2 = new Intent(TwitterActivity.this,EnkoziMainActivity.class);
        startActivity(intent2);
        Log.e("twitter kill","this is the exactly where we need to be!");
    }



    private void retrofitInitializationtwitter(String id, String name, String userEmail, String id_token) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);


        Call<UserInfoJson> call = enkInterface.getTwitteruserlogin(userEmail, id, name, id_token);
        //asynchronous call
        call.enqueue(new retrofit2.Callback<UserInfoJson>() {
            @Override
            public void onResponse(Call<UserInfoJson> call, retrofit2.Response<UserInfoJson> response) {
                int code = response.code();
                if (code == 200) {

                    UserInfoJson userJson = response.body();
                    Log.e("response", "Got the user: " + userJson.getUserInfo());//, Toast.LENGTH_LONG).show();
                    Log.e("response", "added json");
                    if (userJson.getStatus().equals("1")) {
                        //Toast.makeText(EnkoziMainActivity.this, "Signup to cat", Toast.LENGTH_LONG).show();
                        setUser(userJson);
                        usr.saveUserInfo(getApplicationContext());
                        Intent intent = new Intent(TwitterActivity.this, CategSelActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(getApplication(), "Error from Facebook Login", Toast.LENGTH_SHORT);
                    }


                } else {

                    Log.e("Error", "Could not load url");
                }

            }

            @Override
            public void onFailure(Call<UserInfoJson> call, Throwable t) {
                Log.e("Error", "api call fail");

            }
        });



    }

    public void deletedata(){
       /* String email = "";
        String id = "" ;
        String name = "";
        String token = "";*/
        /*getSharedPreferences(PREFS_TWITTER_NAME, MODE_PRIVATE)
                .edit()
                .putString(PREF_Email, email)
                .putString(PREF_Id, id)
                .putString(PREF_NAme, name)
                .putString(PREF_Token, token)
                .commit();*/
        SharedPreferences preferences = getSharedPreferences(PREFS_TWITTER_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("email","");
        edit.putString("id","");
        edit.putString("username","");
        edit.putString("token","");
        edit.commit();
    }

    public void logoutTwitter() {
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            ClearCookies(getApplicationContext());
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();

        }
    }

    public static void ClearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void setTwitterData(String id, String name, String userEmail, String twitter_token) {


        getSharedPreferences(PREFS_TWITTER_NAME, MODE_PRIVATE)
                .edit()
                .putString(PREF_Email, userEmail)
                .putString(PREF_Id, id)
                .putString(PREF_NAme, name)
                .putString(PREF_Token, twitter_token)
                .commit();


    }
    public void getTwitterData() {
        SharedPreferences pref = getSharedPreferences(PREFS_TWITTER_NAME, MODE_PRIVATE);
        String email = pref.getString(PREF_Email, "");
        String id = pref.getString(PREF_Id, "");
        String name=pref.getString(PREF_NAme,"");
        String token=pref.getString(PREF_Token,"");
        retrofitInitializationtwitter(id,name,email,token);


    }


}


