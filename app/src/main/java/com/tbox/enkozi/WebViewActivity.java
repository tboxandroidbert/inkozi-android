package com.tbox.enkozi;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
    }
public void onResume(){
    super.onResume();
    webView = (WebView) findViewById(R.id.web_accountInfo);
    Bundle bundle = getIntent().getExtras();
    String link = bundle.getString("link");
    webView.setWebViewClient(new MyBrowser());
    webView.getSettings().setLoadsImagesAutomatically(true);
    int SDK_INT = android.os.Build.VERSION.SDK_INT;
    if (SDK_INT > 17) {
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
    }
    webView.getSettings().setJavaScriptEnabled(true);
    webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    webView.loadUrl(link);
}
    @Override
    public void onPause(){
        super.onPause();
        webView.clearCache(true);
        webView.stopLoading();
        webView.goBack();
    }
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
            webView.stopLoading();
            this.finish();
        } else {
            super.onBackPressed();
            this.finish();
        }
    }

    private class MyBrowser extends WebViewClient {
        //        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

    }

}