package com.tbox.enkozi;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class TermsAndServicesActivity extends Activity {
    TextView tv_term,tv_agrement,tv_signForservice;
    TextView tv_bul1,tv_sub1bul1,tv_sub1bul2,tv_sub1bul3;
    TextView tv_bul2,tv_sub2bul1,tv_sub2bul2,tv_sub2bul3;
    TextView tv_bul3,tv_sub3bul1,tv_sub3bul2;
    TextView tv_bul4,tv_sub4bul1,tv_sub4bul2,tv_sub4bul3,tv_sub4bul4,tv_sub4bul5,tv_sub4bul6,tv_sub4bul7,tv_sub4bul8,tv_sub4bul9,tv_sub4bul10;
    TextView tv_endorsome,tv_subendor1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_services);
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        registerElement();
    }
    protected  void registerElement()
    {
        tv_term=(TextView)findViewById(R.id.termOfUse);
        tv_agrement=(TextView)findViewById(R.id.agrement);
        tv_signForservice=(TextView)findViewById(R.id.sigupForService);
        tv_bul1=(TextView)findViewById(R.id.bullet1);
        tv_sub1bul1=(TextView)findViewById(R.id.subBullet1);
        tv_sub1bul2=(TextView)findViewById(R.id.subBullet2);
        tv_sub1bul3=(TextView)findViewById(R.id.subBullet3);
        tv_bul2=(TextView)findViewById(R.id.bullet2);
        tv_sub2bul1=(TextView)findViewById(R.id.sub2Bullet1);
        tv_sub2bul2=(TextView)findViewById(R.id.sub2Bullet2);
        tv_sub2bul3=(TextView)findViewById(R.id.sub2Bullet3);
        tv_bul3=(TextView)findViewById(R.id.bullet3);
        tv_sub3bul1=(TextView)findViewById(R.id.sub3Bullet1);
        tv_sub3bul2=(TextView)findViewById(R.id.sub3Bullet2);
        tv_bul4=(TextView)findViewById(R.id.bullet4);
        tv_sub4bul1=(TextView)findViewById(R.id.sub4Bullet1);
        tv_sub4bul2=(TextView)findViewById(R.id.sub4Bullet2);
        tv_sub4bul3=(TextView)findViewById(R.id.sub4Bullet3);
        tv_sub4bul4=(TextView)findViewById(R.id.sub4Bullet4);
        tv_sub4bul5=(TextView)findViewById(R.id.sub4Bullet5);
        tv_sub4bul6=(TextView)findViewById(R.id.sub4Bullet6);
        tv_sub4bul7=(TextView)findViewById(R.id.sub4Bullet7);
        tv_sub4bul8=(TextView)findViewById(R.id.sub4Bullet8);
        tv_sub4bul9=(TextView)findViewById(R.id.sub4Bullet9);
        tv_sub4bul10=(TextView)findViewById(R.id.sub4Bullet10);
        tv_endorsome=(TextView)findViewById(R.id.endorsome);
        tv_subendor1=(TextView)findViewById(R.id.subendor1);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd-Th.otf");
//        Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
//        Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        tv_term.setTypeface(helveticaNeueLTThin);




    }


    /*
Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd-Th.otf");
        Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
        Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        txt_ubermanager.setTypeface(helveticaNeueLTThin);
        txt_sign_in_to_manage.setTypeface(helveticaNeueLTMedium);
        tf_password.setTypeface(helveticaNeueLTLight);
        tf_username.setTypeface(helveticaNeueLTLight);
 */
}
