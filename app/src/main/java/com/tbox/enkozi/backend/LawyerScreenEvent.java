package com.tbox.enkozi.backend;

/**
 * Created by apple on 9/9/16.
 */
public class LawyerScreenEvent {
    private final String message;

    public LawyerScreenEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
