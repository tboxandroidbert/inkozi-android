package com.tbox.enkozi.backend;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by apple on 12/07/16.
 */
public class AttorneyData {
    private static final int MODE_PRIVATE = 0;
    public String attorney_id="",lastName="",firstName="",gradeYear="",image="" ,  phone="";
    private static AttorneyData   _instance;
    public Boolean is_Attorney=false;

    public static AttorneyData getInstance(Context context) {
        if (_instance == null)
        {
            _instance = getUser(context);
        }
        return _instance;
    }
    private static AttorneyData getUser(Context context) {
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences("AttData", MODE_PRIVATE);
        String json = prefs.getString("AttData", null);
        if (json != null) {
            AttorneyData attInfo = gson.fromJson(json, AttorneyData.class);
            if (attInfo.phone.length() > 0)
            {
                attInfo.is_Attorney = true;
            }
            return attInfo;
        }
        else
            return new AttorneyData();
    }
    public void saveUserInfo(Context context){
        this.is_Attorney=true;
        SharedPreferences prefs = context.getSharedPreferences("AttData",MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(this);
        prefsEditor.putString("AttData", json);
        prefsEditor.commit();
    }
    public void removeAttorneyInfo(Context context){
        SharedPreferences prefs = context.getSharedPreferences("AttData",MODE_PRIVATE);
        gradeYear="";
        firstName="";
        lastName="";
        phone="";
        attorney_id="";
        Gson gson = new Gson();
        String json = gson.toJson(this);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("UserData", json);
        prefsEditor.commit();
    }

}
