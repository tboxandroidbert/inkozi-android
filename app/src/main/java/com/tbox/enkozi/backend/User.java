package com.tbox.enkozi.backend;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by apple on 02/07/16.
 */
public class User {
    private static final int MODE_PRIVATE = 0;
    public String user_id="",username="",first_name="",last_name="",email="" ,  phone="",
    api_secret_token="";
    private static User   _instance;
    public Boolean is_login=false;

    public static User getInstance(Context context) {
        if (_instance == null)
        {
            _instance = getUser(context);
        }
        return _instance;
    }
    private static User getUser(Context context){
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences("UserData",MODE_PRIVATE);
        String json = prefs.getString("UserData", null);
        if(json != null){
            User userInfo = gson.fromJson(json, User.class);
            if(userInfo.api_secret_token.length() >0){
                userInfo.is_login=true;

            }
            return userInfo;

        }else{
            return new User();
        }
    }
    public void saveUserInfo(Context context){
        this.is_login=true;
        SharedPreferences prefs = context.getSharedPreferences("UserData",MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(this);
        prefsEditor.putString("UserData", json);
        prefsEditor.commit();
    }
    public void removeUserInfo(Context context){
        SharedPreferences prefs = context.getSharedPreferences("UserData",MODE_PRIVATE);
        this.is_login=false;
        this.api_secret_token="";
//        this.password="";
//        this.email_id="";
        last_name="";
        username="";
        user_id="";
        email="";
        phone="";
        Gson gson = new Gson();
        String json = gson.toJson(this);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("UserData", json);
        prefsEditor.commit();
    }

}
