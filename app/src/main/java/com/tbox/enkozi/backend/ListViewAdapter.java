package com.tbox.enkozi.backend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tbox.enkozi.Pojo.Category;
import com.tbox.enkozi.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 27/06/16.
 */
public class ListViewAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList<Category> data=new ArrayList<Category>();
    private TextView tv_item;


    public ListViewAdapter(Context context, int resource, ArrayList<Category> array) {
        super(context,resource);
        this.context = context;
        data=array;
//        for(int i=0; i<array.size(); i++){
//            data.add(array.get(i));
//            Log.e("data of array",data.get(i).getCategoryName());
//        }
    }

    /*
     * (non-Javadoc)
     * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @SuppressLint({ "ViewHolder", "InflateParams" })
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.e("index",position + "");
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/HelveticaNeue-Light.otf");
        View view = null;
//        try {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.catlistlayout, null, true);

            } else {
                view = convertView;
                Log.e("reponse","elsepart");
            }
        tv_item = (TextView) view.findViewById(R.id.tv_Item);
        if (data != null) {
            final Category category = data.get(position);
            if (category != null) {
                tv_item.setText(category.getCategoryName());
                tv_item.setTypeface(typeface);
                Log.e("Category selected",data.get(position).getCategoryId()+
                        " "+data.get(position).getCategoryName());

            }

        }



        return view;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

}
