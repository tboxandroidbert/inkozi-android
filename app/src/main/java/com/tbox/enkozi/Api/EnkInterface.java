package com.tbox.enkozi.Api;

import com.tbox.enkozi.JsonPojo.*;
import com.tbox.enkozi.Pojo.LoginClass;
import com.tbox.enkozi.Pojo.UserInfo;

import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by apple on 28/06/16.
 */
public interface EnkInterface {
//    String ENDPOINT ="http://192.168.100.4/";
//    @GET("inkozi.php?_controller=inkozi&_action=question_categories")
//    Call<CategoryJson> getUser();
//    @POST("inkozi.php?_controller=inkozi&_action=user_login")
//    Call<UserInfoJson>createUser(@Body UserInfoJson user);
//    @POST("inkozi.php?_controller=inkozi&_action=user_registration")
//    Call<UserInfoJson>registerUser(@Body UserInfoJson user);
//
//    @GET("inkozi.php?_controller=inkozi&_action=states_with_cities")
//    Call<StateJson> getStatesWithCities();
//    @GET("inkozi.php?_controller=inkozi&_action=attorneys")
//    Call<AttorneyJson> getAttorneys();
    //===============================================================
//    String ENDPOINT ="https://dev.inkozi.com/api/01/";// old domain
    //===============================================================
// this is the new domain
//===============================================================
    String ENDPOINT ="http://www.inkozi.com/api/01/";
   // String ENDPOINT ="http://www.inkozi.com/api/01/";
    @GET("question_categories/{country_id}{language}")
    Call<CategoryJson> getCategories(@Query("country_id") String country, @Query("language") String lang );
    @FormUrlEncoded
    @POST("user_login")
    //Call<UserInfoJson>createUser(@Body UserInfoJson user);
    //Call<UserInfoJson> getuserlogin(@Body LoginClass user );
    Call<UserInfoJson> getuserlogin(
            @Field("username") String field1, @Field("password") String field2);
    @FormUrlEncoded
    @POST("user_registration")
    Call<Example>registerUser(@Field("username") String field1,@Field("first_name") String field2, @Field("last_name") String field,
                                      @Field("email") String field4, @Field("password") String field5, @Field("phone") String field6);/*, @Field("country") String field7*/
//    @POST("user_registration")
//    Call<UserInfoJson>registerUser(@Field("username") String field1,@Field("first_name") String field2, @Field("last_name") String field,
//                                       @Field("email") String field4, @Field("password") String field5, @Field("phone") String field6);/*, @Field("country") String field7*/
    @GET("states_with_cities/{language}")
    Call<StateJson>getStatesWithCities(@Query("language") String lang);

    @GET("attorneys/{city_id}{category_id}")
    Call<AttorneyJson> getAttorneys(@Query("city_id") String city, @Query("category_id") String cat_id );
    @GET("category_detail/{category_id}{language}")
    Call<CategoryInfoJson>getQuestions(@Query("category_id") String cat_id,@Query("language") String lang);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<ForgetPasswordJson>getPassword(@Field("username") String email,@Field("language") String lang);
    @FormUrlEncoded
    @POST("forgot_username")
    Call<ForgetUserJson>getUserName(@Field("email") String email,@Field("language") String lang);
    @GET("inkozi_video")
    Call<UrlJson> getUrl();
    @FormUrlEncoded
    @POST("user_login")
    Call<UserInfoJson> getFbuserlogin(
            @Field("email") String email, @Field("id") String id,  @Field("name") String name,  @Field("fb_token") String fb_token);

    @FormUrlEncoded
    @POST("user_login")
    Call<UserInfoJson> getGoogleuserlogin(
            @Field("email") String email, @Field("id") String id,  @Field("name") String name,  @Field("id_token") String id_token);

    @FormUrlEncoded
    @POST("user_login")
    Call<UserInfoJson> getTwitteruserlogin(
            @Field("email") String email, @Field("id") String id,  @Field("name") String name,  @Field("id_token") String id_token);

//    @FormUrlEncoded
//    @POST("emailapi")
//    Call<Questions> getQuestionResponse(
//            @Field("user_id") String user_id, @Field("advisor_id") String advisor_id,  @Field("ques_1") String ques_1,  @Field("ques_2") String ques_2, @Field("ques_3") String ques_3);
@FormUrlEncoded
@POST("emailapi")
Call<Questions> getQuestionResponse(
        @Field("user_id") String user_id, @Field("advisor_id") String advisor_id,  @Field("ques") String ques_1);


}

