package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.ForgetPasswordJson;
import com.tbox.enkozi.JsonPojo.ForgetUserJson;

import java.util.Locale;
import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgetUserActivity extends Activity implements View.OnClickListener,Callback<ForgetUserJson>  {
    TextView tv_forgetText;
    EditText et_Email;
    Button btn_submit;
    Button btn_cancel;
    Button btn_submitClick;
    Button btn_cancelClick;
    TextView tv_term;
    TextView tv_privacacy;
    Button btn_back;
    TextView tv_inkozi;
    private RelativeLayout back_button;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_user);
        registerElement();
        setListner();
    }
    protected void registerElement()
    {
        back_button = (RelativeLayout) findViewById(R.id.back_button);
        tv_forgetText=(TextView)findViewById(R.id.forgetTv);
        tv_privacacy=(TextView)findViewById(R.id.tv_privacy);
        tv_term=(TextView)findViewById(R.id.tv4);
        et_Email=(EditText)findViewById(R.id.emailET);
        btn_submit=(Button)findViewById(R.id.forgetSubmit);
        btn_cancel=(Button)findViewById(R.id.forgetCancel);
        btn_submitClick=(Button)findViewById(R.id.forgetSubmitClick);
        btn_cancelClick=(Button)findViewById(R.id.forgetCancelClick);
        btn_back=(Button) findViewById(R.id.back);
        tv_inkozi=(TextView) findViewById(R.id.tv5);
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        tv_forgetText.setTypeface(helveticaNeueLTThin);
        tv_privacacy.setTypeface(helveticaNeueLTThin);
        tv_term.setTypeface(helveticaNeueLTThin);
        et_Email.setTypeface(helveticaNeueLTThin);
        tv_inkozi.setTypeface(helveticaNeueLTThin);

    }
    protected void setListner()
    {
        try {
            back_button.setOnClickListener(this);
            tv_term.setOnClickListener(this);
            tv_privacacy.setOnClickListener(this);
            btn_cancel.setOnClickListener(this);
            btn_submit.setOnClickListener(this);
            btn_back.setOnClickListener(this);
        }catch ( Exception exp)
        {
            exp.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tv4:
            {
                Intent intent = new Intent(ForgetUserActivity.this,TermsAndServicesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.tv_privacy:
            {
                Intent intent = new Intent(ForgetUserActivity.this, PrivacyPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.forgetCancel:
            {
                btn_cancel.setVisibility(View.GONE);
                btn_cancel.setVisibility(View.VISIBLE);
                Intent intent = new Intent(ForgetUserActivity.this,EnkoziMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.forgetSubmit:
            {
                if(checkInternetConnection()) {
                    btn_submit.setVisibility(View.GONE);
                    btn_submitClick.setVisibility(View.VISIBLE);
                    forgetUser();
                }else{
                    showInternetDialog();
                }

            }
            break;
            case R.id.back:
            {
                Intent intent = new Intent(ForgetUserActivity.this,ForgetPassUserActivity.class);
                startActivity(intent);
            }
            case R.id.back_button:
            {
                Intent intent = new Intent(ForgetUserActivity.this,ForgetPassUserActivity.class);
                startActivity(intent);
            }
            break;
        }

    }
    private  void forgetUser()
    {
        String email=et_Email.getText().toString();
        if(email.equals(""))
        {

            Toast.makeText(ForgetUserActivity.this,"Please enter your Email",Toast.LENGTH_SHORT).show();

        }
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        // prepare call in Retrofit 2.0
        String country="US";
        String lang= Locale.getDefault().toString();
        String sublang= lang.substring(0,2);
        //String subLang=
        if(sublang.equals("es"))
        {
            lang="es";
        }
        else
        {
            lang="en";
        }
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        Call<ForgetUserJson> call = enkInterface.getUserName(email,lang);
        //asynchronous call
        call.enqueue(this);

    }


    @Override
    public void onResponse(Call<ForgetUserJson> call, Response<ForgetUserJson> response) {
        int code = response.code();
        if (code == 200) {
            ForgetUserJson forgetUserJson = response.body();
            Log.e("response", "Got the password: " + forgetUserJson.getStatus());//, Toast.LENGTH_LONG).show();

            if(forgetUserJson.getStatus().toString().equals("1")) {
                //  Toast.makeText(ForgetPassActivity.this,forgetPasswordJson.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("response","yaahoo ho gya");

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(forgetUserJson.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                builder.setCancelable(true);
                                btn_submitClick.setVisibility(View.GONE);
                                btn_submit.setVisibility(View.VISIBLE);
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            else
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(forgetUserJson.getError().toString())
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                builder.setCancelable(true);
                                //do things
                                btn_submitClick.setVisibility(View.GONE);
                                btn_submit.setVisibility(View.VISIBLE);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                Log.e("response","sahi email daal");
            }


        } else {
            Toast.makeText(this, "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onFailure(Call<ForgetUserJson> call, Throwable t) {
        Log.e("response", "api call failed");
    }
    @Override
    public void onRestart() {
        super.onRestart();
//        When BACK BUTTON is pressed, the activity on the stack is restarted
//        Do what you want on the refresh procedure here
        btn_submitClick.setVisibility(View.GONE);
        btn_submit.setVisibility(View.VISIBLE);
        btn_cancelClick.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.VISIBLE);

    }
    public void hideKeyboard2(View v){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable = true;
        } else {
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                ForgetUserActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // do nothing
                                removeInternetDialog();
                               // finish();
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }
}
