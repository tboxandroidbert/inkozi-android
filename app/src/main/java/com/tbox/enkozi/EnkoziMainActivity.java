package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.CategoryInfoJson;
import com.tbox.enkozi.JsonPojo.UrlJson;
import com.tbox.enkozi.JsonPojo.UserInfoJson;
import com.tbox.enkozi.Pojo.LoginClass;
import com.tbox.enkozi.backend.LawyerScreenEvent;
import com.tbox.enkozi.backend.RuntimePermissionHelper;
import com.tbox.enkozi.backend.User;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.facebook.FacebookSdk;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import static com.tbox.enkozi.TwitterActivity.PREFS_TWITTER_NAME;

public class EnkoziMainActivity extends FragmentActivity implements View.OnClickListener, Callback<UserInfoJson>, GoogleApiClient.OnConnectionFailedListener {

    // for twitter=======================================
    private static final String TWITTER_KEY = "zrb2bffNXMTfWbYwqpU9G3r3t";
    private static final String TWITTER_SECRET = "ejEXGoI2Hh5PL6JFw3KugCYslfDsKdNhaldxbeDJRKU2OHJW4C";
    //===================================================
    EditText et_admin;
    EditText et_pass;
    Button btn_remUcheck;
    Button btn_remCheck;
    Button btn_signin;
    Button btn_signup;
    Button btn_facebook, btn_tweet,  btn_youtube ,btn_google;
    public static final String PREFS_TWITTER_NAME = "MyPrefsTwitterFile";
    SignInButton GoogleButton;
    ImageView btn_conAsGuest;
    ImageView btn_forgetpass;
    TextView tv_InfoAdmin;
    TextView tv_InfoPass;
    ImageView im_userInfo;
    ImageView im_passInfo;
    StringBuilder str_builder;
    String str_admin;
    String str_pasword;
    TextView tv_signIn;
    TextView tv_priv;
    TextView tv_term;
    TextView tv_inkozi;
    static Boolean guestFlag = false;
    User usr = null;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    public static final String PREFS_NAME = "MyPrefsFile";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_PASSWORD = "password";
    boolean flag_rememberme ;
    static boolean flag_fbLogin = false;
    static boolean flag_googleLogin = false;
    RelativeLayout userNameInfoClick;
    RelativeLayout passInfoClick;
    RelativeLayout contGuest;


    public static final String PREFS_FB_NAME = "MyPrefsFBFile";
    private static final String PREF_Email = "email";
    private static final String PREF_Id = "id";
    private static final String PREF_NAme = "username";
    private static final String PREF_Token = "token";
   // private WebView webview;

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;
    GoogleSignInOptions gso;
    private static String RememberMe ;
    private String userName;
    private String pass;
    private boolean hint;
    RuntimePermissionHelper runtimePermissionHelper;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onRestart();
        // for facebook------------------------
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());

        // integrating google

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(EnkoziMainActivity.this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.tbox.enkozi",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }




        // end google

        EventBus myEventBus = EventBus.getDefault();
        EventBus.getDefault().register(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override

            public void onSuccess(LoginResult loginResult) {
                AccessToken.getCurrentAccessToken();
                final String fb_token = loginResult.getAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        // Bundle bFacebookData = getFacebookData(object);
                        try {
                            String res = response.toString();
                            Log.e("Response", res);
                            String userEmail = object.getString("email");
                            String id = object.getString("id");
                            String first = object.getString("first_name");
                            String last = object.getString("last_name");
                            String name= first+" "+last;
                            retrofitInitializationfb(id, name, userEmail, fb_token);
                            setFBData(id, first, last, userEmail, fb_token);
                            flag_fbLogin=true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {

                Log.e("fb response ID: ", "cancel clicked");

            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                Log.e("fb response ID: ", "error form api");

            }
        });
        //---------------------------------------------
        // for twitter======================================
        setContentView(R.layout.activity_enkozi_main);
        usr = User.getInstance(this);
        registerViews();
        setListner();
        checkIfLogin();
       // checkIfRemember();
        //retrofitInitializationVideoLink();


        checkPermissions();
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.tbox.enkozi",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkIfRemember();
  //      btn_remCheck.performClick();
       /* try {
            Bundle value = getIntent().getExtras();
            String Signout = (String) value.get("SignOut");
            String BACK = (String) value.get("back_button");
            if (Signout.equals("signout")) {
                onRestart();
            }else if(BACK.equals("backPressed")){
                onRestart();
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/



//        SharedPreferences pref = getSharedPreferences("RememberMePref" ,Context.MODE_PRIVATE);
//        String remember = pref.getString(RememberMe,"");
//        if(remember.equals("true")){
//            SharedPreferences getPref =  getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
//            userName =   getPref.getString(PREF_USERNAME, "");
//            pass =       getPref.getString(PREF_PASSWORD, "");
//            et_admin.setText(userName);
//            et_pass.setText(pass);
//            btn_remCheck.performClick();
//
//        } else if(remember.equals("false")) {
//            et_admin.setText("");
//            et_pass.setText("");
//        }

    }



    private void checkIfLogin() {
        if (usr.is_login) {
            Intent home = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
            startActivity(home);
            finish();
        }
    }

    private void registerViews() {
        btn_google = (Button) findViewById(R.id.googleplus);
        et_admin = (EditText) findViewById(R.id.user_name);
        et_pass = (EditText) findViewById(R.id.user_passwd);
        btn_remCheck = (Button) findViewById(R.id.btn_remchk);
        btn_remUcheck = (Button) findViewById(R.id.btn_remunchk);
        btn_signin = (Button) findViewById(R.id.btn_sigin);
        btn_signup = (Button) findViewById(R.id.btn_sigup);
        btn_facebook = (Button) findViewById(R.id.btn_fb);
        btn_tweet = (Button) findViewById(R.id.btn_tweet);
        GoogleButton = (SignInButton) findViewById(R.id.btn_gPlus);
        btn_youtube = (Button) findViewById(R.id.btn_youtube);
        btn_conAsGuest = (ImageView) findViewById(R.id.bt_contGuest);
        btn_forgetpass = (ImageView) findViewById(R.id.forget_pass);
        tv_InfoAdmin = (TextView) findViewById(R.id.text_adminInfo);
        tv_InfoPass = (TextView) findViewById(R.id.text_passInfoInfo);
        im_userInfo = (ImageView) findViewById(R.id.info_usr);
        im_passInfo = (ImageView) findViewById(R.id.info_pass);
        tv_InfoAdmin.setVisibility(View.INVISIBLE);
        tv_InfoPass.setVisibility(View.INVISIBLE);
        tv_signIn = (TextView) findViewById(R.id.tv1);
        tv_term = (TextView) findViewById(R.id.tv4_main);
        tv_priv = (TextView) findViewById(R.id.tv_privacy);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        tv_inkozi = (TextView) findViewById(R.id.tv5);
//        webview =(WebView) findViewById(R.id.webView1);
//        webview.setWebViewClient(new MyBrowser());
         loginButton.setReadPermissions("email");
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        //Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
        //Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        tv_signIn.setTypeface(helveticaNeueLTThin);
        tv_InfoAdmin.setTypeface(helveticaNeueLTThin);
        tv_InfoPass.setTypeface(helveticaNeueLTThin);
        tv_inkozi.setTypeface(helveticaNeueLTThin);
        GoogleButton.setSize(SignInButton.SIZE_STANDARD);
        GoogleButton.setScopes(gso.getScopeArray());
        userNameInfoClick = (RelativeLayout) findViewById(R.id.userNameInfoClick);
        passInfoClick = (RelativeLayout) findViewById(R.id.passInfoCLick);
        contGuest = (RelativeLayout) findViewById(R.id.contGuest);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void setListner() {
        try {
            btn_signup.setOnClickListener(this);
            btn_signin.setOnClickListener(this);
            btn_remUcheck.setOnClickListener(this);
            btn_remCheck.setOnClickListener(this);
            btn_forgetpass.setOnClickListener(this);
            btn_facebook.setOnClickListener(this);
            btn_tweet.setOnClickListener(this);
            btn_google.setOnClickListener(this);
            btn_youtube.setOnClickListener(this);
       //     btn_conAsGuest.setOnClickListener(this);
            tv_term.setOnClickListener(this);
            tv_priv.setOnClickListener(this);
            im_userInfo.setOnClickListener(this);
            im_passInfo.setOnClickListener(this);
            tv_InfoAdmin.setOnClickListener(this);
            tv_InfoPass.setOnClickListener(this);
    //        userNameInfoClick.setOnClickListener(this);
    //        passInfoClick.setOnClickListener(this);
            contGuest.setOnClickListener(this);

        } catch (Exception exp) {
            exp.printStackTrace();

        }

    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    public void onClick(View view) {
        switch (view.getId()) {
         /*   case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.disconnect_button:
                revokeAccess();
                break;*/
            case R.id.btn_sigup: {
                Intent signupIntent = new Intent(EnkoziMainActivity.this, SignUpActivity.class);
                signupIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(signupIntent);
                //this.finish();
            }
            break;
            case R.id.btn_remchk: {

                flag_rememberme = false;
                setRememberMe();

                btn_remCheck.setVisibility(View.GONE);
                btn_remUcheck.setVisibility(View.VISIBLE);

                DontRememberMe();

            }
            break;
            case R.id.btn_remunchk: {
                flag_rememberme = true;
                setRememberMe();

                btn_remUcheck.setVisibility(View.GONE);
                btn_remCheck.setVisibility(View.VISIBLE);


                RememberMe();


            }
            break;
            case R.id.info_usr: {

              //  im_userInfo.performClick();
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signin_notification)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.info_pass: {

                //  tv_InfoPass.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signin_notification)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();

            }
            break;
            case R.id.btn_fb: {
                if (checkInternetConnection()) {
                    if (flag_fbLogin) {
                        getFBData();
                    } else {
                        loginButton.performClick();

                    }
                } else {
                    showInternetDialog();
                }
            }
            break;
            case R.id.login_button: {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile,email"));

            }
            break;
            case R.id.btn_tweet: {
                SharedPreferences preferences = getSharedPreferences(PREFS_TWITTER_NAME, Context.MODE_PRIVATE);
                String email = preferences.getString("email","");
                if(email.length() ==0)
                {
                    Intent intent = new Intent(EnkoziMainActivity.this, TwitterActivity.class);
                    startActivity(intent);
                }
                else
                {

                    String token= preferences.getString("token","");
                    String name =preferences.getString("username","");
                    String id =preferences.getString("id","");
                    retrofitInitializationtwitter(id,name,email,token);


                }
//                if(PREFS_TWITTER_NAME!=null){
//                    Intent intent = new Intent(EnkoziMainActivity.this, TwitterActivity.class);
//                    startActivity(intent);
//
//                    //        retrofitInitializationtwitter(getid,getname,getemail,getidtoken);
//                }else{
//                    TwitterActivity.getTwitterData();
//
//
//                }

            }
            break;
            case R.id.googleplus:
               /* if (flag_googleLogin) {
                    getGoogleData();
                    signInGoogle();
                }
                else {
                    GoogleButton.performClick();

                }*/
                Log.e("GoogleButton","button has been clicked!");
                GoogleButton.performClick();
            case R.id.btn_gPlus: {
                /*Uri uri = Uri.parse("https://plus.google.com/collections/featured"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/

                Log.e("Enter","You've entered in red zone! ,oh common that's funny");
                if(checkInternetConnection()) {
                    signInGoogle();
                }else{
                    showInternetDialog();
                }
            }
            break;
            case R.id.btn_youtube: {
                //// TODO: 6/16/17 call this api for video link and on positive response to below code there
              //  retrofitInitializationVideoLink();
                if(checkInternetConnection()) {
                    Intent intent = new Intent(EnkoziMainActivity.this, WebViewActivity.class);
                    intent.putExtra("link", "http://inkoziusa.com/videos/Inkozi-Video.mp4");
                    startActivity(intent);
                }else {
                    showInternetDialog();
                }
            }
            break;
            case R.id.contGuest: {
              //  guestFlag = true;
       //         btn_conAsGuest.performClick();
                Intent intent = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
                startActivity(intent);

            }
            break;
            case R.id.forget_pass: {
                //Toast.makeText(EnkoziMainActivity.this, "forget password", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(EnkoziMainActivity.this, ForgetPassUserActivity.class);
                startActivity(intent);

            }
            break;
            case R.id.btn_sigin: {
                if(checkInternetConnection()) {
                    if (isFieldValidated()) {
                        signIn();
                    } else {
                        Toast toast = Toast.makeText(this, getString(R.string.text_enter_all),
                                Toast.LENGTH_SHORT);
                        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                        v.setTextColor(Color.WHITE);
                        toast.show();
                    }
                }else {
                    showInternetDialog();
                }
            }
            break;
            case R.id.tv4_main: {
                Intent intent = new Intent(EnkoziMainActivity.this, TermActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.tv_privacy: {
                Intent intent = new Intent(EnkoziMainActivity.this, PrivacyPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.text_adminInfo: {
                tv_InfoAdmin.setVisibility(View.INVISIBLE);
            }
            break;
            case R.id.text_passInfoInfo: {
                tv_InfoPass.setVisibility(View.INVISIBLE);
            }
            break;



        }
    }

    private void RememberMe(){


            SharedPreferences preferences = getSharedPreferences("RememberMePref",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(RememberMe,"true");
            editor.commit();



    }

    private void DontRememberMe(){
        SharedPreferences preferences = getSharedPreferences("RememberMePref",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(RememberMe,"false");
        editor.commit();
    }


    public void onResponse(Call<UserInfoJson> call, Response<UserInfoJson> response) {
        int code = response.code();
        if (code == 200) {
            UserInfoJson userJson = response.body();
            Log.e("response", "Got the user: " + userJson.getUserInfo());//, Toast.LENGTH_LONG).show();
            Log.e("response", "added json");
            if (userJson.getStatus().equals("1")) {
                //Toast.makeText(EnkoziMainActivity.this, "Signup to cat", Toast.LENGTH_LONG).show();
                setUser(userJson);
                usr.saveUserInfo(this);

                if (guestFlag) {
                    guestFlag = false;
                    getSharedPreferences("BundleReplacementData", Context.MODE_PRIVATE).edit().putString("swapscreen", "yes").commit();
                    Intent intent = new Intent(EnkoziMainActivity.this, LawyerDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                } else {
                    Intent intent = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
                    startActivity(intent);
                    finish();
                }

            } else {
                View view = findViewById(R.id.info_usr);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(userJson.getError())
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }


        } else {
            Toast.makeText(this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onFailure(Call<UserInfoJson> call, Throwable t) {
        Log.e("response", "not added");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    protected void signIn() {
        str_admin = et_admin.getText().toString();
        str_pasword = et_pass.getText().toString();
        LoginClass log = new LoginClass(str_admin, str_pasword);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        Call<UserInfoJson> call = enkInterface.getuserlogin(str_admin, str_pasword);
        //asynchronous call
        call.enqueue(this);
    }


    private boolean isFieldValidated() {
        if (et_admin.getText().length() == 0 || et_pass.getText().length() == 0) {
            return false;
        }
        return true;
    }

    protected void setUser(UserInfoJson infoJson) {
        usr.api_secret_token = infoJson.getApiSecretToken();
        usr.first_name = infoJson.getUserInfo().getFirstName();
        usr.last_name = infoJson.getUserInfo().getLastName();
        // usr.phone = infoJson.getUserInfo().getPhone();
        usr.email = infoJson.getUserInfo().getEmail();
        usr.username = infoJson.getUserInfo().getUsername();
        usr.user_id = infoJson.getUserInfo().getUserId();
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

   /* private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                   //     updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }*/

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

   /* private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.btn_gPlus).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }*/

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                //        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result)  {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String name = acct.getDisplayName();
            String tokenid = acct.getIdToken();
            String email = acct.getEmail();
            String id = acct.getId();

            retrofitInitializationgoogle(id,name,email,id);
       //     setGoogleData(id, name, email, id);
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);

            JSONObject object = new JSONObject();
            try {
                object.put("tokenid",tokenid);
                object.put("name",name);
                object.put("ID",id);
                object.put("email",email);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("Data Fetch",object.toString());

      //      updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
      //      updateUI(false);
            Log.e("Failed","Failed");
        }
    }

    protected void retrofitInitializationVideoLink() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);

        Call<UrlJson> call = enkInterface.getUrl();
        //asynchronous call
        call.enqueue(new Callback<UrlJson>() {
            @Override
            public void onResponse(Call<UrlJson> call, Response<UrlJson> response) {
                int code = response.code();
                if (code == 200) {
                    UrlJson urlJson = response.body();
                    String url = urlJson.getUrl();
                    Log.e("url is :", url);
                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    //// TODO: 6/16/17 call api and load webview activity
                  //  http://inkoziusa.com/videos/Inkozi-Video.mp4
                    startActivity(intent);


//                    webview.getSettings().setLoadsImagesAutomatically(true);
//                    webview.getSettings().setJavaScriptEnabled(true);
//                    webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//                    webview.loadUrl(url);

                } else {

                    Log.e("Error", "Could not load url");
                }

            }

            @Override
            public void onFailure(Call<UrlJson> call, Throwable t) {
                Log.e("Error", "api call fail");

            }
        });
    }


    private void setRememberMe() {
        if (flag_rememberme) {
            getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                    .edit()
                    .putString(PREF_USERNAME, et_admin.getText().toString())
                    .putString(PREF_PASSWORD, et_pass.getText().toString())
                    .apply();
        } else {
            getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                    .edit()
                    .putString(PREF_USERNAME, "")
                    .putString(PREF_PASSWORD, "")
                    .apply();
        }


    }

    private void checkIfRemember() {
        SharedPreferences pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String username = pref.getString(PREF_USERNAME, "");
        String password = pref.getString(PREF_PASSWORD, "");

        if (username.length() > 0 && password.length() > 0) {
            et_admin.setText(username);
            et_pass.setText(password);
            //Prompt for username and password
            btn_remUcheck.setVisibility(View.GONE);
            btn_remCheck.setVisibility(View.VISIBLE);

        }
    }

    public void onEvent(LawyerScreenEvent event) {
        Log.e("string", event.getMessage().toString());
        guestFlag = true;
    }

    private void retrofitInitializationfb(String id, String name, String userEmail, String fb_token) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);


        Call<UserInfoJson> call = enkInterface.getFbuserlogin(userEmail, id, name, fb_token);
        //asynchronous call
        call.enqueue(new Callback<UserInfoJson>() {
            @Override
            public void onResponse(Call<UserInfoJson> call, Response<UserInfoJson> response) {
                int code = response.code();
                if (code == 200) {

                    UserInfoJson userJson = response.body();
                    Log.e("response", "Got the user: " + userJson.getUserInfo());//, Toast.LENGTH_LONG).show();
                    Log.e("response", "added json");
                    if (userJson.getStatus().equals("1")) {
                        //Toast.makeText(EnkoziMainActivity.this, "Signup to cat", Toast.LENGTH_LONG).show();
                        setUser(userJson);
                        usr.saveUserInfo(getApplicationContext());
                        Intent intent = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(getApplication(), getString(R.string.facebook_signin), Toast.LENGTH_SHORT);
                    }


                } else {

                    Log.e("Error", "Could not load url");
                }

            }

            @Override
            public void onFailure(Call<UserInfoJson> call, Throwable t) {
                Log.e("Error", "api call fail");

            }
        });

    }

    private void retrofitInitializationgoogle(String id, String name, String userEmail, String id_token) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);


        Call<UserInfoJson> call = enkInterface.getGoogleuserlogin(userEmail, id, name, id_token);
        //asynchronous call
        call.enqueue(new Callback<UserInfoJson>() {
            @Override
            public void onResponse(Call<UserInfoJson> call, Response<UserInfoJson> response) {
                int code = response.code();
                if (code == 200) {

                    UserInfoJson userJson = response.body();
                    Log.e("response", "Got the user: " + userJson.getUserInfo());//, Toast.LENGTH_LONG).show();
                    Log.e("response", "added json");
                    if (userJson.getStatus().equals("1")) {
                        //Toast.makeText(EnkoziMainActivity.this, "Signup to cat", Toast.LENGTH_LONG).show();
                        setUser(userJson);
                        usr.saveUserInfo(getApplicationContext());
                        Intent intent = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(getApplication(), getString(R.string.google_signin), Toast.LENGTH_SHORT);
                    }


                } else {

                    Log.e("Error", "Could not load url");
                }

            }

            @Override
            public void onFailure(Call<UserInfoJson> call, Throwable t) {
                Log.e("Error", "api call fail");

            }
        });

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(EnkoziMainActivity.this);
        mGoogleApiClient.disconnect();
    }


    private void setFBData(String id, String first, String last, String userEmail, String fb_token) {

        String name = first + " " + last;
        getSharedPreferences(PREFS_FB_NAME, MODE_PRIVATE)
                .edit()
                .putString(PREF_Email, userEmail)
                .putString(PREF_Id, id)
                .putString(PREF_NAme, name)
                .putString(PREF_Token, fb_token)
                .commit();


    }

    private void setGoogleData(String id, String name, String email, String Id) {

      //  String name = first + " " + last;
        getSharedPreferences(PREFS_FB_NAME, MODE_PRIVATE)
                .edit()
                .putString(PREF_Email, email)
                .putString(PREF_Id, id)
                .putString(PREF_NAme, name)
                .putString(PREF_Token, Id)
                .commit();


    }

    private void getFBData() {
        SharedPreferences pref = getSharedPreferences(PREFS_FB_NAME, MODE_PRIVATE);
        String email = pref.getString(PREF_Email, "");
        String id = pref.getString(PREF_Id, "");
        String name=pref.getString(PREF_NAme,"");
        String token=pref.getString(PREF_Token,"");
        retrofitInitializationfb(id,name,email,token);


    }
    private void getGoogleData() {
        SharedPreferences pref = getSharedPreferences(PREFS_FB_NAME, MODE_PRIVATE);
        String email = pref.getString(PREF_Email, "");
        String id = pref.getString(PREF_Id, "");
        String name=pref.getString(PREF_NAme,"");
        String token=pref.getString(PREF_Token,"");
        retrofitInitializationgoogle(id,name,email,token);


    }


//    private class MyBrowser extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//    }


    private void retrofitInitializationtwitter(String id, String name, String userEmail, String fb_token) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);


        Call<UserInfoJson> call = enkInterface.getTwitteruserlogin(userEmail, id, name, fb_token);
        //asynchronous call
        call.enqueue(new Callback<UserInfoJson>() {
            @Override
            public void onResponse(Call<UserInfoJson> call, Response<UserInfoJson> response) {
                int code = response.code();
                if (code == 200) {

                    UserInfoJson userJson = response.body();
                    Log.e("response", "Got the user: " + userJson.getUserInfo());//, Toast.LENGTH_LONG).show();
                    Log.e("response", "added json");
                    if (userJson.getStatus().equals("1")) {
                        //Toast.makeText(EnkoziMainActivity.this, "Signup to cat", Toast.LENGTH_LONG).show();
                        setUser(userJson);
                        usr.saveUserInfo(getApplicationContext());
                        Intent intent = new Intent(EnkoziMainActivity.this, CategSelActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(getApplication(), "Error from Facebook Login", Toast.LENGTH_SHORT);
                    }


                } else {

                    Log.e("Error", "Could not load url");
                }

            }

            @Override
            public void onFailure(Call<UserInfoJson> call, Throwable t) {
                Log.e("Error", "api call fail");

            }
        });

    }

    public void ScreenTap(View v){
        Log.e("Screen","tapped on screen");
        tv_InfoAdmin.setVisibility(View.INVISIBLE);
        tv_InfoPass.setVisibility(View.INVISIBLE);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public void InnerScreenTap(View v){
        Log.e("Inner Screen","tapped on screen");
        tv_InfoAdmin.setVisibility(View.INVISIBLE);
        tv_InfoPass.setVisibility(View.INVISIBLE);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            runtimePermissionHelper = RuntimePermissionHelper.getInstance(this);
            if (runtimePermissionHelper.isAllMCPermissionAvailable()) {
// All permissions available. Go with the flow
                executeTask();
            } else {
// Few permissions not granted. Ask for ungranted permissions
                runtimePermissionHelper.setActivity(this);
                runtimePermissionHelper.requestPermissionsIfDenied(new DialogInterface.OnClickListener() {
                                                                       @Override
                                                                       public void onClick(DialogInterface dialog, int which) {
                                                                           executeTask();
                                                                       }
                                                                   }
                );
            }
        } else {
// SDK below API 23. Do nothing just go with the flow.
            executeTask();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("onRequestPermmision", "in function present");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RuntimePermissionHelper.PERMISSION_REQUEST_CODE) {
            checkPermissions();
        }
    }

    private void executeTask() {

    }

    private boolean checkInternetConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable= true;
        }else{
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                EnkoziMainActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // do nothing
                                removeInternetDialog();
                             //   finish();
                                 android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }
}


/* this code is used to change the text style of textview
Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Th.otf");
        Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
        Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        txt_ubermanager.setTypeface(helveticaNeueLTThin);
        txt_sign_in_to_manage.setTypeface(helveticaNeueLTMedium);
        tf_password.setTypeface(helveticaNeueLTLight);
        tf_username.setTypeface(helveticaNeueLTLight);
 */