package com.tbox.enkozi.location;


import static android.content.Context.LOCATION_SERVICE;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


public class GetLocation implements LocationListener {
    /*  private TextView latituteField;
      private TextView longitudeField;
      private TextView addressField; //Add a new TextView to your activity_main to display the address*/
    private LocationManager locationManager;
    private String provider;
    Context mcontext;
    private String City;
    private String Country;
    private static final int REQUEST_COARSE_LOCATION = 999;
    private static final int REQUEST_FINE_LOCATION = 998;
    Location location;


    public GetLocation(Context context) {
        mcontext = context;

        locationManager = (LocationManager) mcontext.getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
//            android.location.Location location = locationManager.getLastKnownLocation(
//                    LocationManager.GPS_PROVIDER);
           /* if (Build.VERSION.SDK_INT >= 23 && ((ContextCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_COARSE_LOCATION)) != PackageManager.PERMISSION_GRANTED) && ((ContextCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION)) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions((Activity) mcontext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_COARSE_LOCATION );
            } else {

            }*/

            location = locationManager.getLastKnownLocation(provider);
            //          android.location.Location location= locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                System.out.println("Provider " + provider + " has been selected.");
                onLocationChanged(location);
            } else {
                //     latituteField.setText("GetLocation not available");
                //     longitudeField.setText("GetLocation not available");
                Log.i("CurrentLat", "GetLocation not available");
                Log.i("CurrentLong", "GetLocation not available");

            }
        }
    }

    //pending
    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //compiler error on the following line
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location myLocation = locationManager.getLastKnownLocation(provider);
                } else {
                    //Permission denied
                }
                return;
            }
        }
    }*/
   /* @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }*/

    @Override
    public void onLocationChanged(android.location.Location location) {
        //You had this as int. It is advised to have Lat/Loing as double.
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Geocoder geoCoder = new Geocoder(mcontext, Locale.getDefault());
        StringBuilder builder = new StringBuilder();
        try {
            List<Address> address = geoCoder.getFromLocation(lat, lng, 1);
            int maxLines = address.get(0).getMaxAddressLineIndex();
            for (int i=0; i<maxLines+1; i++) {
                String addressStr = address.get(0).getAddressLine(i);
                builder.append(addressStr);
                builder.append(" ");


            }

            String fnialAddress = builder.toString(); //This is the complete address.
            try {

                City = String.valueOf(address.get(0).getLocality());
                Country = String.valueOf(address.get(0).getCountryName());
    //            setCity(City);
    //            setCountry(Country);
            }catch (Exception e){
                e.printStackTrace();
            }
    //        setCity(City);
    //        setCountry(Country);


          //  latituteField.setText(String.valueOf(lat));
          //  longitudeField.setText(String.valueOf(lng));
          //  addressField.setText(fnialAddress); //This will display the final address.
            Log.i("CurrentLat",String.valueOf(lat));
            Log.i("CurrentLong",String.valueOf(lat));
            Log.i("CurrentAddress",String.valueOf(fnialAddress));
        } catch (IOException e) {
            // Handle IOException
        } catch (NullPointerException e) {
            // Handle NullPointerException
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(mcontext, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(mcontext, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    public void setCity(String city) {
        this.City = city;
    }
    public void setCountry(String country) {this.Country = country;}


    public String getCity(){
        return City;
    }
    public String getCountry(){
        return Country;
    }


    private Location getLastKnownLocation() {
        locationManager = (LocationManager)mcontext.getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            else {
                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }
}