package com.tbox.enkozi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.telecom.GatewayInfo;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tbox.enkozi.Api.EnkInterface;
import com.tbox.enkozi.JsonPojo.Example;
import com.tbox.enkozi.JsonPojo.UserInfoJson;
import com.tbox.enkozi.Pojo.RegisterUserInfo;
import com.tbox.enkozi.Pojo.UserInfo;
import com.tbox.enkozi.backend.User;

import java.util.concurrent.TimeUnit;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends Activity implements View.OnClickListener, Callback<Example> {

    EditText et_userName, et_first, et_last;
    EditText et_pass, et_confirmPass;
    EditText et_email, et_conEmail;
    EditText et_number;
    Button btn_signin, btn_submit;
    TextView userTV, firstTv, lastTV, passTV, conPassTv, emailTV, conEmailTV, phTV;
    ImageView img_conGuest;
   // RegisterInfoJson usr = new RegisterInfoJson();
   User usr = null;
    String str_usr,str_first,str_last,str_email,str_pass,str_ph,str_conEmail;
    TextView tv_priv;
    TextView tv_term;
    TextView tv_signup,tv_inkozi;
    Boolean guestFlag=false;
    ImageView im_infoUser,im_infoFirst,im_infoLast,im_infoPass,im_infoConPass,im_infoEmail,im_infoConEmail,im_infophone;
    ProgressDialog ringProgressDialog;
    RelativeLayout userInfoClick , first_nameInfoClick , last_nameInfoClick , emailInfoClick , Con_emailInfoClick ,
            passwordInfoClick , Con_passwordInfoClick , phoneInfoClick ;
    private AlertDialog internetDialog;
    boolean isNetworkAvailable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        registerViews();
        usr = User.getInstance(this);
        setListner();
        visibilityINVISIBLE();
    }

    private void registerViews() {
        et_userName = (EditText) findViewById(R.id.user_name);
        et_first = (EditText) findViewById(R.id.fisrt_name);
        et_last = (EditText) findViewById(R.id.last_name);
        et_pass = (EditText) findViewById(R.id.user_passwd);
        et_confirmPass = (EditText) findViewById(R.id.user_Conpasswd);
        et_email = (EditText) findViewById(R.id.user_email);
        et_conEmail = (EditText) findViewById(R.id.user_conEmail);
        et_number = (EditText) findViewById(R.id.user_ph);
        btn_signin = (Button) findViewById(R.id.btn_sigin);
        img_conGuest = (ImageView) findViewById(R.id.bt_contGuest);
        userTV = (TextView) findViewById(R.id.text_adminInfo);
        firstTv = (TextView) findViewById(R.id.tv_first);
        lastTV = (TextView) findViewById(R.id.tv_last);
        passTV = (TextView) findViewById(R.id.tv_pass);
        conPassTv = (TextView) findViewById(R.id.tv_conpass);
        emailTV = (TextView) findViewById(R.id.tv_email);
        conEmailTV = (TextView) findViewById(R.id.tv_conemail);
        phTV = (TextView) findViewById(R.id.tv_ph);
        btn_submit = (Button) findViewById(R.id.bt_submit);
        tv_signup=(TextView)findViewById(R.id.tv1);
        tv_term=(TextView)findViewById(R.id.tv4);
        tv_priv=(TextView)findViewById(R.id.tv_privacy);
        im_infoUser=(ImageView) findViewById(R.id.info_usr);
        im_infoFirst=(ImageView) findViewById(R.id.info_first);
        im_infoLast=(ImageView) findViewById(R.id.info_last);
        im_infoPass=(ImageView) findViewById(R.id.info_pass);
        im_infoConPass=(ImageView) findViewById(R.id.info_Conpass);
        im_infoEmail=(ImageView) findViewById(R.id.info_email);
        im_infoConEmail=(ImageView) findViewById(R.id.info_Conemail);
        im_infophone=(ImageView) findViewById(R.id.info_ph);
        tv_inkozi=(TextView) findViewById(R.id.tv5);

        Typeface helveticaNeueLTThin = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        //Typeface  helveticaNeueLTLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Lt.otf");
        //Typeface  helveticaNeueLTMedium = Typeface.createFromAsset(getAssets(), "HelveticaNeueLTStd-Md.otf");
        tv_signup.setTypeface(helveticaNeueLTThin);
        et_userName.setTypeface(helveticaNeueLTThin);
        et_number.setTypeface(helveticaNeueLTThin);
        et_last.setTypeface(helveticaNeueLTThin);
        et_conEmail.setTypeface(helveticaNeueLTThin);
        et_confirmPass.setTypeface(helveticaNeueLTThin);
        et_email.setTypeface(helveticaNeueLTThin);
        et_first.setTypeface(helveticaNeueLTThin);
        et_confirmPass.setTypeface(helveticaNeueLTThin);
        et_pass.setTypeface(helveticaNeueLTThin);
        tv_inkozi.setTypeface(helveticaNeueLTThin);
        tv_term.setTypeface(helveticaNeueLTThin);
        tv_priv.setTypeface(helveticaNeueLTThin);
        userInfoClick = (RelativeLayout) findViewById(R.id.userInfoClick);
        first_nameInfoClick = (RelativeLayout) findViewById(R.id.first_nameInfoClick);
        last_nameInfoClick = (RelativeLayout) findViewById(R.id.last_nameInfoClick);
        emailInfoClick = (RelativeLayout) findViewById(R.id.emailInfoClick);
        Con_emailInfoClick = (RelativeLayout) findViewById(R.id.Con_emailInfoClick);
        passwordInfoClick = (RelativeLayout) findViewById(R.id.passwordInfoClick);
        Con_passwordInfoClick = (RelativeLayout) findViewById(R.id.Con_passwordInfoClick);
        phoneInfoClick = (RelativeLayout) findViewById(R.id.phoneInfoClick);

    }

    private void setListner() {
        try {
            btn_submit.setOnClickListener(this);
            btn_signin.setOnClickListener(this);
            img_conGuest.setOnClickListener(this);
            tv_term.setOnClickListener(this);
            tv_priv.setOnClickListener(this);
            im_infoUser.setOnClickListener(this);
            im_infoFirst.setOnClickListener(this);
            im_infoLast.setOnClickListener(this);
            im_infoPass.setOnClickListener(this);
            im_infoConPass.setOnClickListener(this);
            im_infoEmail.setOnClickListener(this);
            im_infoConEmail.setOnClickListener(this);
            im_infophone.setOnClickListener(this);
            userTV.setOnClickListener(this);
            firstTv.setOnClickListener(this);
            lastTV.setOnClickListener(this);
            passTV.setOnClickListener(this);
            conPassTv.setOnClickListener(this);
            emailTV.setOnClickListener(this);
            conEmailTV.setOnClickListener(this);
            phTV.setOnClickListener(this);
            userInfoClick.setOnClickListener(this);
            first_nameInfoClick.setOnClickListener(this);
            last_nameInfoClick.setOnClickListener(this);
            emailInfoClick.setOnClickListener(this);
            Con_emailInfoClick.setOnClickListener(this);
            passwordInfoClick.setOnClickListener(this);
            Con_passwordInfoClick.setOnClickListener(this);
            phoneInfoClick.setOnClickListener(this);
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }

    private void visibilityINVISIBLE() {
        try {
            conEmailTV.setVisibility(View.INVISIBLE);
            emailTV.setVisibility(View.INVISIBLE);
            userTV.setVisibility(View.INVISIBLE);
            firstTv.setVisibility(View.INVISIBLE);
            lastTV.setVisibility(View.INVISIBLE);
            phTV.setVisibility(View.INVISIBLE);
            conPassTv.setVisibility(View.INVISIBLE);
            passTV.setVisibility(View.INVISIBLE);
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sigin:
                Intent signinIntent = new Intent(SignUpActivity.this, EnkoziMainActivity.class);
                signinIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(signinIntent);
            break;
            case R.id.bt_submit: {
                if (isFieldValidated()) {
                    if(checkInternetConnection()){
                        Submit();
                    }else{
                        showInternetDialog(); 
                    }
                    
                }
                else
                {
                    Toast toast= Toast.makeText(this,getString(R.string.text_enter_all),Toast.LENGTH_SHORT);
                    TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                    v.setTextColor(Color.WHITE);
                    toast.show();
                }

            }
            break;
            case R.id.bt_contGuest: {
                guestFlag=true;
                Intent intent = new Intent(SignUpActivity.this,CategSelActivity.class);
                startActivity(intent);
               // Toast.makeText(SignUpActivity.this, "continue as guest", Toast.LENGTH_LONG).show();
            }
            break;
            case R.id.tv4:
            {
                Intent intent = new Intent(SignUpActivity.this,TermsAndServicesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
            break;
            case R.id.tv_privacy:
            {
                Intent intent = new Intent(SignUpActivity.this, PrivacyPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            break;
            case R.id.userInfoClick:
            {
             //   userTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signin_AtInkozi)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();

            }
            break;
            case R.id.text_adminInfo:
            {
                userTV.setVisibility(View.INVISIBLE);
            }
            break;
            case R.id.first_nameInfoClick:
            {
              //  firstTv.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_FnameHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_first:
            {
                firstTv.setVisibility(View.INVISIBLE);
            }
            break;
            case R.id.last_nameInfoClick:
            {
            //    lastTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_LnameHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_last:
            {
                lastTV.setVisibility(View.INVISIBLE);

            }
            break;
            case R.id.emailInfoClick:
            {
             //   emailTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_emailHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_email:
            {
                emailTV.setVisibility(View.INVISIBLE);
            }
              break;
            case R.id.Con_emailInfoClick:
            {
            //    conEmailTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_cemailHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_conemail:
            {
                conEmailTV.setVisibility(View.INVISIBLE);
            }
            break;
            case R.id.passwordInfoClick:
            {
             //   passTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_passHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_pass:
            {
                passTV.setVisibility(View.INVISIBLE);

            }
            break;
            case R.id.Con_passwordInfoClick:
            {
             //   conPassTv.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_cpassHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_conpass:
            {
                conPassTv.setVisibility(View.INVISIBLE);
            }
            break;
            case R.id.phoneInfoClick:
            {
            //    phTV.setVisibility(View.VISIBLE);
                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .text(R.string.signUp_phoneHint)
                        .gravity(Gravity.TOP).backgroundColor(Color.WHITE).textColor(Color.BLACK).arrowColor(Color.WHITE).arrowWidth(15)
                        .build()
                        .show();
            }
            break;
            case R.id.tv_ph:
            {
                phTV.setVisibility(View.INVISIBLE);
            }
            break;
        }
    }

    public void onResponse(Call<Example> call, Response<Example> response) {
        int code = response.code();
        if (code == 200) {
            Example userJson = response.body();
            int status=  userJson.getStatus();
            if(status==0)
            {
                Toast.makeText(this, userJson.getError().toString(), Toast.LENGTH_LONG).show();

            }
            if(status==1 ){
               // UserInfoJson userJson = response.body();

                UserInfo user = userJson.getUserInfo();

                Log.e("response is", response.body().toString());
                Log.e("user is", userJson.toString());
                Log.e("response", "Got the user: " + user.getFirstName());//, Toast.LENGTH_LONG).show();
                Log.e("response", "register json");
                //   Toast.makeText(SignUpActivity.this, "Signup registration done", Toast.LENGTH_LONG).show();
                setUser(user);
                usr.saveUserInfo(getApplicationContext());
                Intent intent = new Intent(SignUpActivity.this, CategSelActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ringProgressDialog.dismiss();
                startActivity(intent);
                finish();
            }
        } else {
            Toast.makeText(SignUpActivity.this, getString(R.string.register_failed), Toast.LENGTH_LONG).show();
            ringProgressDialog.dismiss();
        }

    }

    public void launchRingDialog() {
        ringProgressDialog = ProgressDialog.show(SignUpActivity.this, getString(R.string.pleasewate), getString(R.string.creatingUser), true);
        ringProgressDialog.setCancelable(true);
        ringProgressDialog.show();

    }

    @Override
    public void onFailure(Call<Example> call, Throwable t) {
        Log.e("response", "nope json");
        Log.e("error",t.toString());
        Toast.makeText(SignUpActivity.this, getString(R.string.register_failed), Toast.LENGTH_LONG).show();
        ringProgressDialog.dismiss();

    }
    protected void Submit(){

        str_usr=et_userName.getText().toString();
         str_first=et_first.getText().toString();
         str_last=et_last.getText().toString();
         str_email=et_email.getText().toString();
         str_conEmail=et_conEmail.getText().toString();
         str_pass=et_pass.getText().toString();
         str_ph=et_number.getText().toString();
       // String str_coun=et_coutry
//        str_conPass=et_email.getText().toString();
//        if(!str_email.equals(str_conEmail)) // check email full logic is not used
//        {
//            conEmailTV.setText("Email not same enter again");
//            conEmailTV.setVisibility(View.VISIBLE);
//        }
//        if(!str_pass.equals(str_conPass))// check password full logic is not used
//        {
//            conPassTv.setText("Email not same enter again");
//            conPassTv.setVisibility(View.VISIBLE);
//        }
//        usr.setEmail(et_email.getText().toString());

        //   usr.getPhone(et_number.getText().toString());
        launchRingDialog();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnkInterface.ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        // prepare call in Retrofit 2.0
        EnkInterface enkInterface = retrofit.create(EnkInterface.class);
        Call<Example> call = enkInterface.registerUser(str_usr,str_first,str_last,str_email,str_pass,str_ph);
        //asynchronous call
        call.enqueue(this);

    }
    private boolean isFieldValidated(){
        if(et_first.getText().length()==0 || et_pass.getText().length()==0 || et_last.getText().length()==0 || et_number.getText().length()==0
                || et_conEmail.getText().length()==0|| et_confirmPass.getText().length()==0|| et_last.getText().length()==0|| et_userName.getText().length()==0){
            return false;
        }
        return true;
    }
    protected void setUser(UserInfo infoJson) {

       //usr.api_secret_token = infoJson.getApiSecretToken();
        usr.first_name = infoJson.getFirstName();
        usr.last_name = infoJson.getLastName();
        // usr.phone = infoJson.getUserInfo().getPhone();
        usr.email = infoJson.getEmail();
        usr.username = infoJson.getUsername();
        usr.user_id = infoJson.getUserId();
    }
    public void SignUpScreenTap(View v){
        userTV.setVisibility(View.INVISIBLE);
        firstTv.setVisibility(View.INVISIBLE);
        lastTV.setVisibility(View.INVISIBLE);
        passTV.setVisibility(View.INVISIBLE);
        conPassTv.setVisibility(View.INVISIBLE);
        emailTV.setVisibility(View.INVISIBLE);
        conEmailTV.setVisibility(View.INVISIBLE);
        phTV.setVisibility(View.INVISIBLE);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean checkInternetConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isNetworkAvailable= true;
        }else{
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    private void showInternetDialog() {

        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(
                SignUpActivity.this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // User pressed Cancel button. Write
                                // Logic Here
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // do nothing
                                removeInternetDialog();
                                //finish();
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            internetDialog = null;

        }
    }
}
